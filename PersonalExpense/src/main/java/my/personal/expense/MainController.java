package my.personal.expense;

import java.io.FileOutputStream;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.WorkbookUtil;

@SuppressWarnings("restriction")
public class MainController extends Application {

	static Logger logger = Logger.getLogger(MainController.class.toString());

	public static void main(String[] args) throws Exception {

		logger.info("Staring application");

		launch(args);
		// createWorkbook();

		logger.info("Shutting down application");
	}

	@SuppressWarnings("unused")
	private static void createWorkbook() throws Exception {
		Workbook wb = new HSSFWorkbook(); // or new XSSFWorkbook();
		Sheet sheet1 = wb.createSheet("new sheet");
		Sheet sheet2 = wb.createSheet("second sheet");

		String safeName = WorkbookUtil
				.createSafeSheetName("[O'Brien's sales*?]"); // returns
																// " O'Brien's sales   "
		Sheet sheet3 = wb.createSheet(safeName);

		FileOutputStream fileOut = new FileOutputStream("workbook.xls");
		wb.write(fileOut);
		fileOut.close();
	}

	@Override
	public void start(Stage stage) throws Exception {
		// configure and display the scene and stage.
		stage.setTitle("Personal Expense Utility");
		stage.setScene(new Scene(new PersonalExpenseWizard(stage), 640, 480));
		stage.show();
	}

}
