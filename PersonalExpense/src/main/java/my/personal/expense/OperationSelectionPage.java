package my.personal.expense;

import java.util.logging.Logger;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.VBoxBuilder;

@SuppressWarnings("restriction")
public class OperationSelectionPage extends WizardPage {

	Logger logger = Logger.getLogger(MainController.class.toString());

	private RadioButton opt1;
	private RadioButton opt2;
	private ToggleGroup options = new ToggleGroup();

	public OperationSelectionPage() {
		super("Operations selection");

		nextButton.setDisable(true);
		finishButton.setDisable(true);
		opt1.setToggleGroup(options);
		opt2.setToggleGroup(options);
		options.selectedToggleProperty().addListener(
				new ChangeListener<Toggle>() {
					public void changed(
							ObservableValue<? extends Toggle> observableValue,
							Toggle oldToggle, Toggle newToggle) {
						nextButton.setDisable(false);
						// finishButton.setDisable(false);
					}
				});
	}

	Parent getContent() {
		opt1 = new RadioButton("Create workbook for a certain year");
		opt2 = new RadioButton("Summurize workbook of a certain year");

		return VBoxBuilder.create().spacing(5)
				.children(new Label("What do wish to do?"), opt1, opt2).build();
	}

	void nextPage() {		if (options.getSelectedToggle().equals(opt1)) {
			logger.info("Selection 1: Creating Workbook for a certain year");
			
			// navTo("Workbook year");
			super.nextPage();

		} else if (options.getSelectedToggle().equals(opt2)) {
			logger.info("Selection 2: Summurizing workbook of a certain year");
		} else {
			navTo("Thank You");
		}
	}
}
