package my.personal.expense;

import java.util.logging.Logger;

import javafx.stage.Stage;

@SuppressWarnings("restriction")
public class PersonalExpenseWizard extends Wizard {
	static Logger logger = Logger.getLogger(PersonalExpenseWizard.class
			.toString());
	Stage stage;

	public PersonalExpenseWizard(Stage stage) {
		// super(new ComplaintsPage(), new MoreInformationPage(), new
		// ThanksPage());
		super(new OperationSelectionPage());
		this.stage = stage;
	}

	public void finish() {
		logger.info("Finished");
		stage.close();
	}

	public void cancel() {
		logger.info("Cancelled");
		stage.close();
	}

}
