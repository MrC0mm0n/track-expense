package my.personal.expense;

import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.VBoxBuilder;

@SuppressWarnings("restriction")
public class WorkbookExpenseYearPage extends WizardPage {

	public WorkbookExpenseYearPage() {
		super("Workbook year");

	}

	Parent getContent() {

		return VBoxBuilder
				.create()
				.spacing(5)
				.children(
						new Label(
								"For which year do you wish to create a workbook?"))
				.build();
	}

}
