package expense;

import java.io.Console;
import java.io.IOException;

import jxl.read.biff.BiffException;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ExpenseCodeCaller {

	public static void main(String[] args) throws RowsExceededException,
			WriteException, BiffException, IOException {

		ExpenseCode_V1_19 eX = new ExpenseCode_V1_19();

		eX.methodForInsertingSeparatorToConsole();
		System.out.println("Running code '" + ExpenseCode_V1_19.className
				+ "' \n" + "Creator: Shaik Mohammed Rawoof \n"
				+ "Alias: MrC0mm0n on Twitter/Facebook \n");
		eX.methodForInsertingSeparatorToConsole();
		System.out.println("Source Monthly Expense WB name: "
				+ eX.getSourceXLSFileName(0) + "\n"
				+ "Source Yearly Expense WB name: "
				+ eX.getSourceXLSFileNameYearly() + "\n"
				+ "Test Monthly Expense WB name: "
				+ eX.getDestnXLSFileName(0) + "\n"
				+ "Test Yearly Expense WB name: "
				+ eX.getDestnXLSFileNameYearly(0) + "\n"
				+ "Test Expense Till Date WB name: "
				+ eX.getDestnXLSFileNameExpenseTillDate());
		eX.methodForInsertingSeparatorToConsole();

		String[] stringArray = {};
		eX.main(stringArray);

	}
}
