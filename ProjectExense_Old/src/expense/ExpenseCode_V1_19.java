package expense;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.BorderLineStyle;
import jxl.format.CellFormat;
import jxl.format.Colour;
import jxl.format.PageOrientation;
import jxl.format.PaperSize;
import jxl.format.UnderlineStyle;
import jxl.format.BoldStyle;
import jxl.read.biff.BiffException;
import jxl.write.Border;
import jxl.write.Formula;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableFont.FontName;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

public class ExpenseCode_V1_19 {
	static Label L;
	static Number N;
	static Formula F;
	static String buf;
	static String className = "ExpenseCode_V1_19";
	static BufferedReader bufferRead = new BufferedReader(
			new InputStreamReader(System.in));
	static String[] firstColumnDetails = { "Days", "Expenses", "Grocery",
			"Nece./Shop./Uten./Cloth.", "Entertainment", "Health/Self-Care",
			"Books/Self-improvement", "Travel", "Fuel",
			"Vehicle/Maint./Parking", "Cell Recharge", "Internet",
			"Restaurants", "Rent/Utilities/Maint.", "Fin./Edu./Post. Exp.",
			"Insurance/PF", "Tax/Charity", "Luxury/Treats/Gifts/Eidee",
			"Maint./Fines/Other", "TDE", "EPD", "EPW", "Income", "Savings",
			"% SWRTI" };
	static String[] firstColumnDetailsYear = { "Month", "January", "February",
			"March", "April", "May", "June", "July", "August", "September",
			"October", "November", "December", "Total" };
	static String[] firstColumnDetailsYearOlderSheets = { "Days", "Expenses",
			"Laundry", "Necessities/Entertainment", "Education", "Travel",
			"Cell Recharge", "Restaurants", "Other", "TDE", "EPD", "EPW",
			"Income", "Savings", "% SWRTI" };
	static String[] firstRowDetailsYear = { "Month", "Job Income",
			"Extra Income", "Total Income", "Personal Expense",
			"Extra Expense", "Total Expense", "Savings" };
	static String[] firstRowDetailsExpenseTillDate = { "Year", "Total Income",
			"Total Expense", "Savings" };
	static String[] firstColumnDetailsYearPCE = { "PCE", "January", "February",
			"March", "April", "May", "June", "July", "August", "September",
			"October", "November", "December", "Total PCE" };

	public static void main(String[] args) throws RowsExceededException,
			NumberFormatException, BiffException, WriteException, IOException {
		for (int consoleCounter = 0; consoleCounter > -1; consoleCounter++) {

			System.out
					.println("Enter '1' to start performing excel operations and anything else to exit.");

			String consoleScanner = bufferRead.readLine();

			if (consoleScanner.matches("1")) {
				// Ask user whether to create/modify/rearrange sheets in
				// ExcelBook.
				WritableWorkbook wworkB = performOperations();

				// Workbook write and close.
				wworkB.write();
				wworkB.close();

				System.out.println("Completed Operation Number : "
						+ (consoleCounter + 1));
			} else {
				System.out.println("Operation(s) done and exiting.");
				System.exit(0);
			}
		}

	}

	private static WritableWorkbook performOperations()
			throws NumberFormatException, IOException, BiffException,
			RowsExceededException, WriteException {

		WritableWorkbook wworkB = null;
		methodForInsertingSeparatorToConsole();
		System.out
				.println("What to you want to do? \n"
						+ "Enter: \n"
						+ "1 :Create a new expense workbook. \n"
						+ "2 :Add a new month's expense sheet to an existing expense workbook. \n"
						+ "3 :Create a year's expense sheet in the desired expense workbook. \n"
						+ "4 :Create an expenditure-till-date workbook. \n");
		int conditionToArrangeSheets = Integer.parseInt(bufferRead.readLine());
		int year, month;
		switch (conditionToArrangeSheets) {
		case 1:
			// Creating a month's expense sheet in a new workbook
			year = getYearFromConsole();
			month = getMonthFromConsole();
			wworkB = createANewMonthlyExpenseWorkbook(year);
			System.out.println("Creating a new expense workbook.");
			insertNewSheetMonthly(month, year, wworkB);
			break;
		case 2:
			// Modify an existing workbook
			year = getYearFromConsole();
			month = getMonthFromConsole();
			wworkB = addSheetsToAnExistingWorkbook(year);
			System.out
					.println("Adding a new month's expense sheet to an existing expense workbook.");
			insertNewSheetMonthly(month, year, wworkB);
			break;

		case 3: // Creating new year's expense sheet to a a new workbook
			year = getYearFromConsole();
			wworkB = addANewYearlyExpenseSheet(year);
			System.out
					.println("Creating a year's expense sheet in the desired expense workbook.");
			insertNewSheetYearly(year, wworkB);
			break;

		case 4: // Create a new expenditure-till-date expense sheet to a new
			// workbook.
			wworkB = createANewWorkbookExpenseTillDate();
			System.out.println("Creating an expenditure-till-date workbook.");
			insertNewSheetExpenseTillDate(wworkB);
			break;

		default:
			System.out.println("***********EXITING PROGRAM***********");
			System.exit(0);
			break;
		}
		return wworkB;

	}

	private static void insertNewSheetExpenseTillDate(WritableWorkbook wworkB)
			throws NumberFormatException, IOException, WriteException,
			BiffException {

		String sheetName = "Expense Till Date";

		WritableSheet excelSheet = createANewSheetAtIndex0OftheSpecifiedWorkbook(
				sheetName, wworkB);

		insertDetailsDataFormulaeInSheetExpenseTillDate(excelSheet);

	}

	private static void insertDetailsDataFormulaeInSheetExpenseTillDate(
			WritableSheet excelSheet) throws BiffException, IOException,
			WriteException {

		Workbook readableWB = getReadableCopyOfWB(getDestinationPath(),
				getSourceXLSFileNameYearly());

		fillDetailsInTheFirstColumnExpenseTillDate(readableWB, excelSheet);

		fillDetailsInTheFirstRowExpenseTillDate(excelSheet);

		fillDataInTheMiddleRegionExpenseTillDate(readableWB, excelSheet);

		addFormulaeToSumTheNumbersExpenseTillDate(readableWB, excelSheet);

		makeTheNecessarySettingsToTheSheetYear("Expenses Till Date", excelSheet);

	}

	private static void addFormulaeToSumTheNumbersExpenseTillDate(
			Workbook readableWB, WritableSheet excelSheet)
			throws WriteException {

		int rowTotalExpenseTillDate = readableWB.getNumberOfSheets() + 1;

		buf = "SUM(B2:B" + rowTotalExpenseTillDate + ")";
		F = new Formula(1, rowTotalExpenseTillDate, buf,
				getCellFormatForMiddleAlignmentBorderTopBottomRight(
						Colour.GREY_25_PERCENT, Colour.BLACK));
		excelSheet.addCell(F);

		buf = "SUM(C2:C" + rowTotalExpenseTillDate + ")";
		F = new Formula(2, rowTotalExpenseTillDate, buf,
				getCellFormatForMiddleAlignmentBorderTopBottomRight(
						Colour.GREY_25_PERCENT, Colour.BLACK));
		excelSheet.addCell(F);

		buf = "SUM(D2:D" + rowTotalExpenseTillDate + ")";
		F = new Formula(3, rowTotalExpenseTillDate, buf,
				getCellFormatForMiddleAlignmentBorderTopBottomRight(
						Colour.GREY_25_PERCENT, Colour.BLACK));
		excelSheet.addCell(F);

	}

	private static void fillDataInTheMiddleRegionExpenseTillDate(
			Workbook readableWB, WritableSheet excelSheet)
			throws WriteException {

		Sheet[] readableSheets = readableWB.getSheets();
		String[] Income = getIncomeFromYearlyExpense(readableSheets);
		String[] Expense = getExpenseFromYearlyExpense(readableSheets);
		String[] Savings = getSavingsFromYearlyExpense(readableSheets);
		double data;

		for (int column = 1; column < firstRowDetailsExpenseTillDate.length; column++) {
			for (int row = 1; row < readableSheets.length + 1; row++) {
				switch (column) {
				case 1:
					data = Double.parseDouble(Income[row - 1]);
					N = new Number(column, row, data,
							getCellFormatForMiddleAlignmentBorderRight(
									Colour.WHITE, Colour.BLACK));
					excelSheet.addCell(N);
					break;
				case 2:
					data = Double.parseDouble(Expense[row - 1]);
					N = new Number(column, row, data,
							getCellFormatForMiddleAlignmentBorderRight(
									Colour.WHITE, Colour.BLACK));
					excelSheet.addCell(N);
					break;
				case 3:
					data = Double.parseDouble(Savings[row - 1]);
					N = new Number(column, row, data,
							getCellFormatForMiddleAlignmentBorderRight(
									Colour.WHITE, Colour.BLACK));
					excelSheet.addCell(N);
					break;
				}
			}
		}

	}

	private static String[] getSavingsFromYearlyExpense(Sheet[] readableSheets) {
		String[] Savings = new String[readableSheets.length];

		for (int numSheet = 0, count = readableSheets.length - 1; numSheet < readableSheets.length; numSheet++) {

			int rowTotal = readableSheets[numSheet].findCell("Total").getRow();
			int columnSavings = readableSheets[numSheet].findCell("Savings")
					.getColumn();

			Savings[count - numSheet] = readableSheets[numSheet].getCell(
					columnSavings, rowTotal).getContents();
		}

		return Savings;
	}

	private static String[] getIncomeFromYearlyExpense(Sheet[] readableSheets) {
		String[] Income = new String[readableSheets.length];

		for (int numSheet = 0, count = readableSheets.length - 1; numSheet < readableSheets.length; numSheet++) {

			int rowTotal = readableSheets[numSheet].findCell("Total").getRow();
			int columnTotalIncome = readableSheets[numSheet].findCell(
					"Total Income").getColumn();

			Income[count - numSheet] = readableSheets[numSheet].getCell(
					columnTotalIncome, rowTotal).getContents();
		}

		return Income;
	}

	private static String[] getExpenseFromYearlyExpense(Sheet[] readableSheets) {
		String[] Expense = new String[readableSheets.length];

		for (int numSheet = 0, count = readableSheets.length - 1; numSheet < readableSheets.length; numSheet++) {

			int rowTotal = readableSheets[numSheet].findCell("Total").getRow();
			int columnTotalExpense = readableSheets[numSheet].findCell(
					"Total Expense").getColumn();

			Expense[count - numSheet] = readableSheets[numSheet].getCell(
					columnTotalExpense, rowTotal).getContents();
		}

		return Expense;
	}

	private static void fillDetailsInTheFirstRowExpenseTillDate(
			WritableSheet excelSheet) throws WriteException {

		for (int column = 0; column < firstRowDetailsExpenseTillDate.length; column++) {

			L = new Label(
					column,
					0,
					firstRowDetailsExpenseTillDate[column],
					getCellFormatForMiddleRegion(Colour.AUTOMATIC, Colour.WHITE));
			excelSheet.addCell(L);

		}

	}

	private static void fillDetailsInTheFirstColumnExpenseTillDate(
			Workbook readableWB, WritableSheet excelSheet)
			throws WriteException {

		String[] sheetNames = readableWB.getSheetNames();

		for (int row = 0, count = sheetNames.length - 1; row < sheetNames.length; row++) {
			L = new Label(0, (row + 1), sheetNames[count - row], getCellFormat(
					Colour.AUTOMATIC, Colour.WHITE));
			excelSheet.addCell(L);
			if (row == sheetNames.length - 1) {
				L = new Label(0, (row + 2), "Total", getCellFormat(
						Colour.AUTOMATIC, Colour.WHITE));
				excelSheet.addCell(L);
			}
		}

	}

	public static String getSourceXLSFileNameYearly() {
		// Hard-coded Value
		String sourceFileName = "Yearly Expenses.xls";

		// Getting the file name from the console.
		// methodForInsertingSeparatorToConsole();
		// System.out.println("Enter the name of your source file:");
		// String sourceFileName = bufferRead.readLine() + ".xls";

		return sourceFileName;
	}

	private static WritableWorkbook createANewWorkbookExpenseTillDate()
			throws IOException {
		WritableWorkbook wworkB = Workbook.createWorkbook(new File(
				getDestinationPath() + getDestnXLSFileNameExpenseTillDate()));

		return wworkB;
	}

	public static String getDestnXLSFileNameExpenseTillDate() {
		// Hard-coded Value
		String destnFileName = className + "_Expenses Till Date.xls";

		// Getting the file name from the console.
		// methodForInsertingSeparatorToConsole();
		// System.out.println("Enter a name for your destination file:");
		// String destnFileName = bufferRead.readLine() + ".xls";

		return destnFileName;
	}

	private static void insertNewSheetYearly(int year, WritableWorkbook wworkB)
			throws NumberFormatException, IOException, WriteException,
			BiffException {

		String sheetName = getSheetName(year);

		WritableSheet excelSheet = createANewSheetAtIndex0OftheSpecifiedWorkbook(
				sheetName, wworkB);

		insertDetailsDataFormulaeInSheetYear(year, excelSheet);

		makeTheNecessarySettingsToTheSheetYear(sheetName, excelSheet);

	}

	private static void insertDetailsDataFormulaeInSheetYear(int year,
			WritableSheet excelSheet) throws WriteException, BiffException,
			IOException {

		Workbook readableWB = getReadableCopyOfWB(
				getPathOfTheMonthlyExpenseWB(),
				getTheNameOfTheMonthlyExpenseWB(year));

		fillDetailsInTheFirstColumnYear(excelSheet);

		fillDetailsInTheFirstRowYear(excelSheet);

		fillDataInTheMiddleRegionYear(year, excelSheet, readableWB);

		fillFormulaeToSumIncomeExpenseSavings(excelSheet);

		addPerCategoryExpensesOverEntireYear(year, excelSheet, readableWB);

	}

	private static void addPerCategoryExpensesOverEntireYear(int year,
			WritableSheet excelSheet, Workbook readableWB)
			throws WriteException {

		int rowPCE = 17;

		// Adding details to row
		for (int column = 0; column < firstColumnDetails.length; column++) {
			if (column > 1 && column < 19) {
				L = new Label(column - 1, rowPCE, firstColumnDetails[column],
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == 19) {
				L = new Label(column - 1, rowPCE, "TME",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			}
		}

		// Adding details to column
		for (int row = rowPCE; row < rowPCE + firstColumnDetailsYearPCE.length; row++) {
			if (row != rowPCE) {
				L = new Label(0, row, firstColumnDetailsYearPCE[row - rowPCE],
						getCellFormat(Colour.AUTOMATIC, Colour.WHITE));
				excelSheet.addCell(L);
			} else {
				L = new Label(0, row, firstColumnDetailsYearPCE[row - rowPCE],
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			}
		}

		// Adding formulae for PCE and TME
		int rowTotalPCE = rowPCE + firstColumnDetailsYearPCE.length - 1;
		int columnTME = 18;
		// PCE
		char character = 'B';
		for (int column = 1; column < columnTME; column++, character++) {
			buf = "SUM(" + character + (rowPCE + 1 + 1) + ":" + character
					+ rowTotalPCE + ")";
			F = new Formula(column, rowTotalPCE, buf,
					getCellFormatForMiddleAlignmentBorderTopBottom(
							Colour.GREY_25_PERCENT, Colour.BLACK));
			excelSheet.addCell(F);
		}
		// TME
		for (int row = rowPCE + 1; row < rowTotalPCE + 1; row++) {
			if (row != rowTotalPCE) {
				buf = "SUM(B" + (row + 1) + ":R" + (row + 1) + ")";
				F = new Formula(columnTME, row, buf,
						getCellFormatForMiddleAlignmentBorderRightLeft(
								Colour.GREY_25_PERCENT, Colour.BLACK));
				excelSheet.addCell(F);
			} else {
				buf = "SUM(B" + (row + 1) + ":R" + (row + 1) + ")";
				F = new Formula(columnTME, row, buf,
						getCellFormatForMiddleAlignmentBorderAll(
								Colour.GREY_25_PERCENT, Colour.BLACK));
				excelSheet.addCell(F);
			}
		}

		// Middle region data
		for (int i = 0; i < readableWB.getNumberOfSheets(); i++) {
			System.out.println(readableWB.getSheet(i).getCell("EPD").getContents().toString());
		}
	}

	private static CellFormat getCellFormatForMiddleAlignmentBorderAll(
			Colour colourBack, Colour colourFont) throws WriteException {
		WritableFont fontFormat = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				colourFont);

		WritableCellFormat cellFormat = new WritableCellFormat(fontFormat);
		cellFormat.setBackground(colourBack);
		cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.BLACK);
		cellFormat.setAlignment(Alignment.CENTRE);

		return cellFormat;
	}

	private static CellFormat getCellFormatForMiddleAlignmentBorderRightLeft(
			Colour colourBack, Colour colourFont) throws WriteException {
		WritableFont fontFormat = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				colourFont);

		WritableCellFormat cellFormat = new WritableCellFormat(fontFormat);
		cellFormat.setBackground(colourBack);
		cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
		cellFormat.setBorder(Border.LEFT, BorderLineStyle.THIN, Colour.BLACK);
		cellFormat.setAlignment(Alignment.CENTRE);

		return cellFormat;
	}

	private static void fillFormulaeToSumIncomeExpenseSavings(
			WritableSheet excelSheet) throws WriteException {

		int rowTotal = excelSheet.findCell("Total").getRow();
		char character = 'B';

		for (int column = 1; column < firstRowDetailsYear.length; column++, character++) {
			switch (column) {
			case 1:
				buf = "SUM(" + character + "2:" + character + "13)";
				F = new Formula(column, rowTotal, buf,
						getCellFormatForMiddleAlignmentBorderTopBottom(
								Colour.GREY_25_PERCENT, Colour.BLACK));
				excelSheet.addCell(F);
				break;
			case 2:
				buf = "SUM(" + character + "2:" + character + "13)";
				F = new Formula(column, rowTotal, buf,
						getCellFormatForMiddleAlignmentBorderTopBottom(
								Colour.GREY_25_PERCENT, Colour.BLACK));
				excelSheet.addCell(F);
				break;
			case 3:
				buf = "SUM(" + character + "2:" + character + "13)";
				F = new Formula(column, rowTotal, buf,
						getCellFormatForMiddleAlignmentBorderTopBottomRight(
								Colour.GREY_25_PERCENT, Colour.BLACK));
				excelSheet.addCell(F);
				break;
			case 4:
				buf = "SUM(" + character + "2:" + character + "13)";
				F = new Formula(column, rowTotal, buf,
						getCellFormatForMiddleAlignmentBorderTopBottom(
								Colour.GREY_25_PERCENT, Colour.BLACK));
				excelSheet.addCell(F);
				break;
			case 5:
				buf = "SUM(" + character + "2:" + character + "13)";
				F = new Formula(column, rowTotal, buf,
						getCellFormatForMiddleAlignmentBorderTopBottom(
								Colour.GREY_25_PERCENT, Colour.BLACK));
				excelSheet.addCell(F);
				break;
			case 6:
				buf = "SUM(" + character + "2:" + character + "13)";
				F = new Formula(column, rowTotal, buf,
						getCellFormatForMiddleAlignmentBorderTopBottomRight(
								Colour.GREY_25_PERCENT, Colour.BLACK));
				excelSheet.addCell(F);
				break;
			case 7:
				buf = "SUM(" + character + "2:" + character + "13)";
				F = new Formula(column, rowTotal, buf,
						getCellFormatForMiddleAlignmentBorderTopBottomRight(
								Colour.GREY_25_PERCENT, Colour.BLACK));
				excelSheet.addCell(F);
				break;
			}
		}
	}

	private static Workbook getReadableCopyOfWB(String path, String XLSFileName)
			throws BiffException, IOException {
		Workbook readableWB = Workbook
				.getWorkbook(new File(path + XLSFileName));

		return readableWB;

	}

	private static String getTheNameOfTheMonthlyExpenseWB(int year)
			throws IOException {
		// Hard-coded Value
		String FileName = "Expenses " + year + ".xls";
		System.out.println("Name of expense file: " + FileName);

		// Getting the file name from the console.
		// methodForInsertingSeparatorToConsole();
		// System.out.println("Enter a name for your monthly expense file:");
		// String FileName = bufferRead.readLine() + ".xls";

		return FileName;
	}

	private static String getPathOfTheMonthlyExpenseWB() throws IOException {
		// Hard-coded Value
		String Path = "";

		// Getting the path from the console.
		// methodForInsertingSeparatorToConsole();
		// System.out
		// .println("Enter the directory where your monthly expense file is:");
		// String Path = bufferRead.readLine() + "\\";

		Path = Path.replace("\\", "/");

		return Path;
	}

	private static void fillDataInTheMiddleRegionYear(int year,
			WritableSheet excelSheet, Workbook readableWB)
			throws BiffException, IOException, WriteException {

		String[] Income = getTheIncomeFromMonthlyExpense(year, readableWB);
		String[] Expense = getTheExpenseFromMonthlyExpense(year, readableWB);
		double data;

		for (int i = 0; i < Income.length; i++) {
			if (Income[i] == null) {
				Income[i] = "0";
			}
			if (Expense[i] == null) {
				Expense[i] = "0";
			}
		}

		for (int column = 1; column < firstRowDetailsYear.length; column++) {
			for (int row = 1; row < firstColumnDetailsYear.length - 1; row++) {
				switch (column) {
				case 1:
					data = Double.parseDouble(Income[row - 1]);
					N = new Number(column, row, data,
							getCellFormatForMiddleRegion(Colour.WHITE,
									Colour.BLACK));
					excelSheet.addCell(N);
					break;
				case 2:
					data = 0;
					N = new Number(column, row, data,
							getCellFormatForMiddleRegion(Colour.WHITE,
									Colour.BLACK));
					excelSheet.addCell(N);
					break;
				case 3:
					row++;
					buf = "B" + row + "+C" + row;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignmentBorderRight(
									Colour.WHITE, Colour.BLACK));
					excelSheet.addCell(F);
					break;
				case 4:
					data = Double.parseDouble(Expense[row - 1]);
					N = new Number(column, row, data,
							getCellFormatForMiddleRegion(Colour.WHITE,
									Colour.BLACK));
					excelSheet.addCell(N);
					break;
				case 5:
					data = 0;
					N = new Number(column, row, data,
							getCellFormatForMiddleRegion(Colour.WHITE,
									Colour.BLACK));
					excelSheet.addCell(N);
					break;
				case 6:
					row++;
					buf = "E" + row + "+F" + row;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignmentBorderRight(
									Colour.WHITE, Colour.BLACK));
					excelSheet.addCell(F);
					break;
				case 7:
					row++;
					buf = "D" + row + "-G" + row;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignmentBorderRight(
									Colour.WHITE, Colour.BLACK));
					excelSheet.addCell(F);
					break;
				}
			}
		}

	}

	private static String[] getTheIncomeFromMonthlyExpense(int year,
			Workbook readableWB) {
		int numberOfSheets = readableWB.getNumberOfSheets();

		String yearChar = Integer.toString(year);

		String[] renameSheet = readableWB.getSheetNames();
		String[] Income = new String[12];

		for (int numSheet = 0; numSheet < numberOfSheets; numSheet++) {

			if (renameSheet[numSheet].contains(yearChar)) {
				for (int i = 1; i < firstColumnDetailsYear.length; i++) {
					if (renameSheet[numSheet]
							.contains(firstColumnDetailsYear[i])) {

						Sheet dump;
						Cell dumpCell;
						int dumpCellRow;

						dump = readableWB.getSheet(numSheet);
						dumpCell = dump.findCell("Income");
						dumpCellRow = dumpCell.getRow();
						dumpCell = dump.getCell(1, dumpCellRow);
						Income[i - 1] = dumpCell.getContents();
					}
				}
			}
		}

		return Income;
	}

	private static String[] getTheExpenseFromMonthlyExpense(int year,
			Workbook readableWB) {
		int numberOfSheets = readableWB.getNumberOfSheets();

		String yearChar = Integer.toString(year);

		String[] readableSheet = readableWB.getSheetNames();
		String[] Expense = new String[12];

		for (int numSheet = 0; numSheet < numberOfSheets; numSheet++) {

			if (readableSheet[numSheet].contains(yearChar)) {
				for (int i = 1; i < firstColumnDetailsYear.length; i++) {
					if (readableSheet[numSheet]
							.contains(firstColumnDetailsYear[i])) {

						Sheet dump;
						int dumpCellTME;
						int dumpCellTDE;

						dump = readableWB.getSheet(numSheet);
						dumpCellTME = dump.findCell("TME").getColumn();
						dumpCellTDE = dump.findCell("TDE").getRow();
						Expense[i - 1] = dump.getCell(dumpCellTME, dumpCellTDE)
								.getContents();
					}
				}
			}
		}

		return Expense;
	}

	private static void fillDetailsInTheFirstRowYear(WritableSheet excelSheet)
			throws WriteException {

		for (int column = 0; column < firstRowDetailsYear.length; column++) {

			L = new Label(
					column,
					0,
					firstRowDetailsYear[column],
					getCellFormatForMiddleRegion(Colour.AUTOMATIC, Colour.WHITE));
			excelSheet.addCell(L);

		}

	}

	private static void fillDetailsInTheFirstColumnYear(WritableSheet excelSheet)
			throws WriteException {

		for (int row = 0; row < firstColumnDetailsYear.length; row++) {

			L = new Label(0, row, firstColumnDetailsYear[row], getCellFormat(
					Colour.AUTOMATIC, Colour.WHITE));
			excelSheet.addCell(L);

		}
	}

	private static WritableWorkbook addANewYearlyExpenseSheet(int year)
			throws IOException, BiffException {
		WritableWorkbook wworkB = Workbook.createWorkbook(new File(
				getDestinationPath() + getDestnXLSFileName(year)), Workbook
				.getWorkbook(new File(getSourcePath()
						+ getSourceXLSFileName(year))));

		return wworkB;
	}

	private static String getSheetName(int year) {

		String sheetName = Integer.toString(year);

		return sheetName;
	}

	public static String getDestnXLSFileNameYearly(int year) {
		// Hard-coded Value
		String destnFileName = className + " Expenses " + year + ".xls";

		// Getting the file name from the console.
		// methodForInsertingSeparatorToConsole();
		// System.out.println("Enter a name for your destination file:");
		// String destnFileName = bufferRead.readLine() + ".xls";

		return destnFileName;
	}

	private static void insertNewSheetMonthly(int month, int year,
			WritableWorkbook wworkB) throws NumberFormatException, IOException,
			RowsExceededException, WriteException {

		// Code to year, month, days and sheet name.
		int days = getDaysInTheSpecifiedMonth(year, month);
		float weeks = getWeeksInTheSpecifiedMonth(days, year, month);

		String sheetName = getSheetName(year, month);

		System.out.println(sheetName);

		WritableSheet excelSheet = createANewSheetAtIndex0OftheSpecifiedWorkbook(
				sheetName, wworkB);

		insertDetailsDataFormulaeInSheet(days, weeks, firstColumnDetails,
				excelSheet);

		// Code for the sheet configuration.
		makeTheNecessarySettingsToTheSheet(days, sheetName, excelSheet);

	}

	private static void makeTheNecessarySettingsToTheSheetYear(
			String sheetName, WritableSheet excelSheet) throws IOException {
		// Adjusting Column view.
		for (int column = 0; column < firstColumnDetailsYear.length; column++) {

			excelSheet.setColumnView(column, 15);

		}

		// Freeze first column

		// Setting sheet orientation.
		excelSheet.setPageSetup(PageOrientation.LANDSCAPE, PaperSize.A4, 0.75,
				0.75);

		String currency = getCurrency();
		String dateTime = getDateAndTime();

		// Setting sheet header.
		excelSheet.setHeader("", sheetName, "");
		excelSheet.setFooter("Generated on: " + dateTime, "", "All amounts in "
				+ currency);

	}

	private static void makeTheNecessarySettingsToTheSheet(int days,
			String sheetName, WritableSheet excelSheet) throws IOException {

		// Adjusting Column view.
		for (int column = 0; column < days + 10; column++) {
			if (column > 1 && column < days + 1) {
				// column size for days
				excelSheet.setColumnView(column, 5);
			} else if (column >= (days + 1)) {
				// column size for last set of rows
				excelSheet.setColumnView(column, 17);
			} else if (column == 0) {
				// column size for first column
				excelSheet.setColumnView(column, 22);
			} else if (column == 1) {
				// column size for second column
				excelSheet.setColumnView(column, 8);
			}
		}

		// Freeze first column

		// Setting sheet orientation.
		excelSheet.setPageSetup(PageOrientation.LANDSCAPE, PaperSize.A3, 0.75,
				0.75);

		String currency = getCurrency();
		String dateTime = getDateAndTime();

		// Setting sheet header.
		excelSheet.setHeader("", sheetName, "");
		excelSheet.setFooter("Generated on: " + dateTime, "", "All amounts in "
				+ currency);
	}

	private static String getDateAndTime() {
		String date = "yyyy-MM-dd HH:mm:ss";

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat(date);
		return dateFormat.format(calendar.getTime());

	}

	private static String getCurrency() throws IOException {
		// String currency = "INR";
		methodForInsertingSeparatorToConsole();
		System.out
				.println("What is the currency you spend in? (e.g. INR, CAD, AED, etc...)");
		String currency = bufferRead.readLine();
		currency = currency.toUpperCase();
		return currency;

	}

	private static void insertDetailsDataFormulaeInSheet(int days, float weeks,
			String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException,
			NumberFormatException, IOException {
		switch (days) {
		case 28:
			if (days == 28) {
				fillDetailsInTheFirstColumn(arrayFirstColumn, excelSheet);

				fillDatesAndDetailsInFirstRow(days, arrayFirstColumn,
						excelSheet);

				fillDataInTheMiddleRegion(days, arrayFirstColumn, excelSheet);

				addFormulaeToSumExpenseDayWise(days, arrayFirstColumn,
						excelSheet);

				// Code to add formulae to calculate expense category-wise,
				// %WRTE, %WRTI.
				addFormulaeToCalcCatWiseWrteWrti_28(days, weeks,
						arrayFirstColumn, excelSheet);

				// Code to add formulae to calculate EPD, %EPD-WRTE, %EPD-WRTI,
				// Savings and %Savings.
				addFormulaeToCalcEPDEPDWRTEEPDWRTI_28(days, weeks,
						arrayFirstColumn, excelSheet);

			}
			break;

		case 29:
			if (days == 29) {
				fillDetailsInTheFirstColumn(arrayFirstColumn, excelSheet);

				fillDatesAndDetailsInFirstRow(days, arrayFirstColumn,
						excelSheet);

				fillDataInTheMiddleRegion(days, arrayFirstColumn, excelSheet);

				addFormulaeToSumExpenseDayWise(days, arrayFirstColumn,
						excelSheet);

				// Code to add formulae to calculate expense category-wise,
				// %WRTE, %WRTI.
				addFormulaeToCalcCatWiseWrteWrti_29(days, weeks,
						arrayFirstColumn, excelSheet);

				// Code to add formulae to calculate EPD, %EPD-WRTE, %EPD-WRTI,
				// Savings and %Savings.
				addFormulaeToCalcEPDEPDWRTEEPDWRTI_29(days, weeks,
						arrayFirstColumn, excelSheet);

			}
			break;

		case 30:
			if (days == 30) {
				fillDetailsInTheFirstColumn(arrayFirstColumn, excelSheet);

				fillDatesAndDetailsInFirstRow(days, arrayFirstColumn,
						excelSheet);

				fillDataInTheMiddleRegion(days, arrayFirstColumn, excelSheet);

				addFormulaeToSumExpenseDayWise(days, arrayFirstColumn,
						excelSheet);

				// Code to add formulae to calculate expense category-wise,
				// %WRTE, %WRTI.
				addFormulaeToCalcCatWiseWrteWrti_30(days, weeks,
						arrayFirstColumn, excelSheet);

				// Code to add formulae to calculate EPD, %EPD-WRTE, %EPD-WRTI,
				// Savings and %Savings.
				addFormulaeToCalcEPDEPDWRTEEPDWRTI_30(days, weeks,
						arrayFirstColumn, excelSheet);

			}
			break;

		case 31:
			if (days == 31) {
				fillDetailsInTheFirstColumn(arrayFirstColumn, excelSheet);

				fillDatesAndDetailsInFirstRow(days, arrayFirstColumn,
						excelSheet);

				fillDataInTheMiddleRegion(days, arrayFirstColumn, excelSheet);

				addFormulaeToSumExpenseDayWise(days, arrayFirstColumn,
						excelSheet);

				// Code to add formulae to calculate expense category-wise,
				// %WRTE, %WRTI.
				addFormulaeToCalcCatWiseWrteWrti_31(days, weeks,
						arrayFirstColumn, excelSheet);

				// Code to add formulae to calculate EPD, %EPD-WRTE, %EPD-WRTI,
				// Savings and %Savings.
				addFormulaeToCalcEPDEPDWRTEEPDWRTI_31(days, weeks,
						arrayFirstColumn, excelSheet);

			}
			break;
		}
	}

	private static void fillDataInTheMiddleRegion(int days,
			String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException {

		int rowTillWhichToFillZero = findTheTDERowNumber(arrayFirstColumn);

		for (int column = 1; column < days + 1; column++) {
			for (int row = 2; row < rowTillWhichToFillZero; row++) {
				N = new Number(column, row, 0,
						getCellFormatForMiddleRegionOfMonthExpense(
								Colour.WHITE, Colour.BLACK));

				excelSheet.addCell(N);
			}
		}
	}

	private static CellFormat getCellFormatForMiddleRegionOfMonthExpense(
			Colour colourBack, Colour colourFont) throws WriteException {
		WritableFont fontFormat = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				colourFont);

		WritableCellFormat cellFormat = new WritableCellFormat(fontFormat);
		cellFormat.setBackground(colourBack);
		cellFormat.setBorder(Border.BOTTOM, BorderLineStyle.NONE, Colour.BLACK);
		cellFormat.setAlignment(Alignment.CENTRE);

		return cellFormat;
	}

	private static CellFormat getCellFormatForMiddleRegion(Colour colourBack,
			Colour colourFont) throws WriteException {

		WritableFont fontFormat = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				colourFont);

		WritableCellFormat cellFormat = new WritableCellFormat(fontFormat);
		cellFormat.setBackground(colourBack);
		// cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN, Colour.WHITE);
		cellFormat.setAlignment(Alignment.CENTRE);

		return cellFormat;
	}

	private static void addFormulaeToCalcEPDEPDWRTEEPDWRTI_31(int days,
			float weeks, String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException,
			NumberFormatException, IOException {
		int rowTDE = findTheTDERowNumber(arrayFirstColumn);
		int rowIncome = findTheIncomeRowNumber(arrayFirstColumn);

		for (int column = 1; column < 2; column++) {
			for (int row = rowTDE; row < firstColumnDetails.length; row++) {
				if (row == (rowTDE + 1)) {

					// addFormulaeToCalcEPD
					buf = "(AG" + (rowTDE + 1) + "/" + days + ")";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 2)) {

					// add Formulae to Calc EPW
					buf = "(AG" + (rowTDE + 1) + "/" + weeks + ")";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 3)) {

					// Asking user to enter his current month's income.
					int salary = getSpecifiedMonthsSalary();
					N = new Number(column, row, salary,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(N);

				} else if (row == (rowTDE + 4)) {

					// addFormualeToClacSavings
					buf = "B" + (rowIncome + 1) + "-AG" + (rowTDE + 1);
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 5)) {

					// addFormualeToClac%Savings
					buf = "(B" + (rowIncome + 2) + "/B" + (rowIncome + 1)
							+ ")*100";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				}
			}
		}
	}

	private static void addFormulaeToCalcCatWiseWrteWrti_31(int days,
			float weeks, String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException {
		int rowTDE = findTheTDERowNumber(arrayFirstColumn) + 1;
		int rowIncome = findTheIncomeRowNumber(arrayFirstColumn) + 1;
		for (int column = days + 1; column < days + 10; column++) {
			for (int row = 2; row < rowTDE; row++) {
				if (column == days + 1) {

					// "TME-PC"
					row++;
					buf = "SUM(B" + row + ":AF" + row + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 2) {

					// "EPW-PC"
					row++;
					buf = "(AG" + row + "/" + weeks + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 3) {

					// "EPD-PC"
					row++;
					buf = "(AG" + row + "/" + days + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 4) {

					// "% TME-PC-WRTE"
					row++;
					buf = "(AG" + row + "/AG" + rowTDE + ")*100";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 5) {

					// "% TME-PC-WRTI"
					row++;
					buf = "(AG" + row + "/B" + rowIncome + ")*100";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 6) {

					// "% EPW-PC-WRTE"
					row++;
					buf = "((AG" + row + "/AG" + rowTDE + ")*100)/" + weeks;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 7) {

					// "% EPW-PC-WRTI"
					row++;
					buf = "((AG" + row + "/B" + rowIncome + ")*100/" + weeks;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 8) {

					// "% EPD-PC-WRTI"
					row++;
					buf = "((AG" + row + "/AG" + rowTDE + ")*100)/" + days;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 9) {

					// "% EPD-PC-WRTE"
					row++;
					buf = "((AG" + row + "/B" + rowIncome + ")*100/" + days;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				}
			}
		}
	}

	private static void addFormulaeToCalcEPDEPDWRTEEPDWRTI_30(int days,
			float weeks, String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException,
			NumberFormatException, IOException {
		int rowTDE = findTheTDERowNumber(arrayFirstColumn);
		int rowIncome = findTheIncomeRowNumber(arrayFirstColumn);
		for (int column = 1; column < 2; column++) {
			for (int row = rowTDE; row < firstColumnDetails.length; row++) {
				if (row == (rowTDE + 1)) {

					// addFormulaeToCalcEPD
					buf = "(AF" + (rowTDE + 1) + "/" + days + ")";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 2)) {

					// add Formulae to Calc EPW
					buf = "(AF" + (rowTDE + 1) + "/" + weeks + ")";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 3)) {

					// Asking user to enter his current month's income.
					int salary = getSpecifiedMonthsSalary();
					N = new Number(column, row, salary,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(N);

				} else if (row == (rowTDE + 4)) {

					// addFormualeToClacSavings
					buf = "B" + (rowIncome + 1) + "-AF" + (rowTDE + 1);
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 5)) {

					// addFormualeToClac%Savings
					buf = "(B" + (rowIncome + 2) + "/B" + (rowIncome + 1)
							+ ")*100";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				}
			}
		}
	}

	private static void addFormulaeToCalcCatWiseWrteWrti_30(int days,
			float weeks, String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException {
		int rowTDE = findTheTDERowNumber(arrayFirstColumn) + 1;
		int rowIncome = findTheIncomeRowNumber(arrayFirstColumn) + 1;
		for (int column = days + 1; column < days + 10; column++) {
			for (int row = 2; row < rowTDE; row++) {
				if (column == days + 1) {

					// "TME-PC"
					row++;
					buf = "SUM(B" + row + ":AE" + row + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 2) {

					// "EPW-PC"
					row++;
					buf = "(AF" + row + "/" + weeks + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 3) {

					// "EPD-PC"
					row++;
					buf = "(AF" + row + "/" + days + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 4) {

					// "% TME-PC-WRTE"
					row++;
					buf = "(AF" + row + "/AF" + rowTDE + ")*100";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 5) {

					// "% TME-PC-WRTI"
					row++;
					buf = "(AF" + row + "/B" + rowIncome + ")*100";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 6) {

					// "% EPW-PC-WRTE"
					row++;
					buf = "((AF" + row + "/AF" + rowTDE + ")*100)/" + weeks;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 7) {

					// "% EPW-PC-WRTI"
					row++;
					buf = "((AF" + row + "/B" + rowIncome + ")*100/" + weeks;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 8) {

					// "% EPD-PC-WRTI"
					row++;
					buf = "((AF" + row + "/AF" + rowTDE + ")*100)/" + days;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 9) {

					// "% EPD-PC-WRTE"
					row++;
					buf = "((AF" + row + "/B" + rowIncome + ")*100/" + days;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				}
			}
		}
	}

	private static void addFormulaeToCalcEPDEPDWRTEEPDWRTI_29(int days,
			float weeks, String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException,
			NumberFormatException, IOException {
		int rowTDE = findTheTDERowNumber(arrayFirstColumn);
		int rowIncome = findTheIncomeRowNumber(arrayFirstColumn);
		for (int column = 1; column < 2; column++) {
			for (int row = rowTDE; row < firstColumnDetails.length; row++) {
				if (row == (rowTDE + 1)) {

					// addFormulaeToCalcEPD
					buf = "(AE" + (rowTDE + 1) + "/" + days + ")";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 2)) {

					// add Formulae to Calc EPW
					buf = "(AE" + (rowTDE + 1) + "/" + weeks + ")";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 3)) {

					// Asking user to enter his current month's income.
					int salary = getSpecifiedMonthsSalary();
					N = new Number(column, row, salary,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(N);

				} else if (row == (rowTDE + 4)) {

					// addFormualeToClacSavings
					buf = "B" + (rowIncome + 1) + "-AE" + (rowTDE + 1);
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 5)) {

					// addFormualeToClac%Savings
					buf = "(B" + (rowIncome + 2) + "/B" + (rowIncome + 1)
							+ ")*100";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				}
			}
		}
	}

	private static void addFormulaeToCalcCatWiseWrteWrti_29(int days,
			float weeks, String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException {
		int rowTDE = findTheTDERowNumber(arrayFirstColumn) + 1;
		int rowIncome = findTheIncomeRowNumber(arrayFirstColumn) + 1;
		for (int column = days + 1; column < days + 10; column++) {
			for (int row = 2; row < rowTDE; row++) {
				if (column == days + 1) {

					// "TME-PC"
					row++;
					buf = "SUM(B" + row + ":AD" + row + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 2) {

					// "EPW-PC"
					row++;
					buf = "(AE" + row + "/" + weeks + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 3) {

					// "EPD-PC"
					row++;
					buf = "(AE" + row + "/" + days + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 4) {

					// "% TME-PC-WRTE"
					row++;
					buf = "(AE" + row + "/AE" + rowTDE + ")*100";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 5) {

					// "% TME-PC-WRTI"
					row++;
					buf = "(AE" + row + "/B" + rowIncome + ")*100";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 6) {

					// "% EPW-PC-WRTE"
					row++;
					buf = "((AE" + row + "/AE" + rowTDE + ")*100)/" + weeks;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 7) {

					// "% EPW-PC-WRTI"
					row++;
					buf = "((AE" + row + "/B" + rowIncome + ")*100/" + weeks;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 8) {

					// "% EPD-PC-WRTI"
					row++;
					buf = "((AE" + row + "/AE" + rowTDE + ")*100)/" + days;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 9) {

					// "% EPD-PC-WRTE"
					row++;
					buf = "((AE" + row + "/B" + rowIncome + ")*100/" + days;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				}
			}
		}
	}

	private static void addFormulaeToCalcEPDEPDWRTEEPDWRTI_28(int days,
			float weeks, String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException,
			NumberFormatException, IOException {
		int rowTDE = findTheTDERowNumber(arrayFirstColumn);
		int rowIncome = findTheIncomeRowNumber(arrayFirstColumn);
		for (int column = 1; column < 2; column++) {
			for (int row = rowTDE; row < firstColumnDetails.length; row++) {
				if (row == (rowTDE + 1)) {

					// addFormulaeToCalcEPD
					buf = "(AD" + (rowTDE + 1) + "/" + days + ")";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 2)) {

					// add Formulae to Calc EPW
					buf = "(AD" + (rowTDE + 1) + "/" + weeks + ")";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 3)) {

					// Asking user to enter his current month's income.
					int salary = getSpecifiedMonthsSalary();
					N = new Number(column, row, salary,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(N);

				} else if (row == (rowTDE + 4)) {

					// addFormualeToClacSavings
					buf = "B" + (rowIncome + 1) + "-AD" + (rowTDE + 1);
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				} else if (row == (rowTDE + 5)) {

					// addFormualeToClac%Savings
					buf = "(B" + (rowIncome + 2) + "/B" + (rowIncome + 1)
							+ ")*100";
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(Colour.LIGHT_GREEN,
									Colour.BLACK));
					excelSheet.addCell(F);

				}
			}
		}
	}

	private static void addFormulaeToCalcCatWiseWrteWrti_28(int days,
			float weeks, String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException {
		int rowTDE = findTheTDERowNumber(arrayFirstColumn) + 1;
		int rowIncome = findTheIncomeRowNumber(arrayFirstColumn) + 1;
		for (int column = days + 1; column < days + 10; column++) {
			for (int row = 2; row < rowTDE; row++) {
				if (column == days + 1) {

					// "TME-PC"
					row++;
					buf = "SUM(B" + row + ":AC" + row + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 2) {

					// "EPW-PC"
					row++;
					buf = "(AD" + row + "/" + weeks + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 3) {

					// "EPD-PC"
					row++;
					buf = "(AD" + row + "/" + days + ")";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 4) {

					// "% TME-PC-WRTE"
					row++;
					buf = "(AD" + row + "/AD" + rowTDE + ")*100";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 5) {

					// "% TME-PC-WRTI"
					row++;
					buf = "(AD" + row + "/B" + rowIncome + ")*100";
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 6) {

					// "% EPW-PC-WRTE"
					row++;
					buf = "((AD" + row + "/AD" + rowTDE + ")*100)/" + weeks;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 7) {

					// "% EPW-PC-WRTI"
					row++;
					buf = "((AD" + row + "/B" + rowIncome + ")*100/" + weeks;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 8) {

					// "% EPD-PC-WRTI"
					row++;
					buf = "((AD" + row + "/AD" + rowTDE + ")*100)/" + days;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				} else if (column == days + 9) {

					// "% EPD-PC-WRTE"
					row++;
					buf = "((AD" + row + "/B" + rowIncome + ")*100/" + days;
					row--;
					F = new Formula(column, row, buf,
							getCellFormatForMiddleAlignment(
									Colour.GREY_25_PERCENT, Colour.BLACK));
					excelSheet.addCell(F);

				}
			}
		}
	}

	private static void addFormulaeToSumExpenseDayWise(int days,
			String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException {

		char columnChar = 'B';
		char columnChar_2 = 'A';

		int rowTDE = findTheTDERowNumber(arrayFirstColumn);

		for (int column = 1; column < days + 1; column++) {
			if (column < 26) {
				buf = "SUM(" + columnChar + "3:" + columnChar + rowTDE + ")";
				F = new Formula(column, rowTDE, buf,
						getCellFormatForMiddleAlignment(Colour.GREY_25_PERCENT,
								Colour.BLACK));
				excelSheet.addCell(F);
			} else {
				buf = "SUM(A" + columnChar_2 + "3:A" + columnChar_2 + rowTDE
						+ ")";
				F = new Formula(column, rowTDE, buf,
						getCellFormatForMiddleAlignment(Colour.GREY_25_PERCENT,
								Colour.BLACK));
				excelSheet.addCell(F);
				columnChar_2++;
			}
			columnChar++;
		}
	}

	private static void fillDatesAndDetailsInFirstRow(int days,
			String[] arrayFirstColumn, WritableSheet excelSheet)
			throws RowsExceededException, WriteException {

		for (int column = 1; column < days + 10; column++) {
			String date = Integer.toString(column);
			if (column < days + 1) {
				L = new Label(column, 0, date, getCellFormatForMiddleAlignment(
						Colour.AUTOMATIC, Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == days + 1) {
				L = new Label(column, 0, "TME",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == days + 2) {
				L = new Label(column, 0, "EPW-PC",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == days + 3) {
				L = new Label(column, 0, "EPD-PC",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == days + 4) {
				L = new Label(column, 0, "% TME-PC-WRTE",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == days + 5) {
				L = new Label(column, 0, "% TME-PC-WRTI",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == days + 6) {
				L = new Label(column, 0, "% EPW-PC-WRTE",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == days + 7) {
				L = new Label(column, 0, "% EPW-PC-WRTI",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == days + 8) {
				L = new Label(column, 0, "% EPD-PC-WRTE",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			} else if (column == days + 9) {
				L = new Label(column, 0, "% EPD-PC-WRTI",
						getCellFormatForMiddleAlignment(Colour.AUTOMATIC,
								Colour.WHITE));
				excelSheet.addCell(L);
			}
		}
	}

	private static CellFormat getCellFormatForMiddleAlignmentBorderTopBottomRight(
			Colour colourBack, Colour colourFont) throws WriteException {
		WritableFont fontFormat = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				colourFont);

		WritableCellFormat cellFormat = new WritableCellFormat(fontFormat);
		cellFormat.setBackground(colourBack);
		cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
		cellFormat.setBorder(Border.TOP, BorderLineStyle.THIN, Colour.BLACK);
		cellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
		cellFormat.setAlignment(Alignment.CENTRE);

		return cellFormat;
	}

	private static CellFormat getCellFormatForMiddleAlignmentBorderTopBottom(
			Colour colourBack, Colour colourFont) throws WriteException {
		WritableFont fontFormat = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				colourFont);

		WritableCellFormat cellFormat = new WritableCellFormat(fontFormat);
		cellFormat.setBackground(colourBack);
		cellFormat.setBorder(Border.TOP, BorderLineStyle.THIN, Colour.BLACK);
		cellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THIN, Colour.BLACK);
		cellFormat.setAlignment(Alignment.CENTRE);

		return cellFormat;
	}

	private static CellFormat getCellFormatForMiddleAlignmentBorderRight(
			Colour colourBack, Colour colourFont) throws WriteException {
		WritableFont fontFormat = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				colourFont);

		WritableCellFormat cellFormat = new WritableCellFormat(fontFormat);
		cellFormat.setBackground(colourBack);
		cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THIN, Colour.BLACK);
		cellFormat.setAlignment(Alignment.CENTRE);

		return cellFormat;
	}

	private static CellFormat getCellFormatForMiddleAlignment(
			Colour colourBack, Colour colourFont) throws WriteException {

		WritableFont fontFormat = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				colourFont);

		WritableCellFormat cellFormat = new WritableCellFormat(fontFormat);
		cellFormat.setBackground(colourBack);
		cellFormat.setAlignment(Alignment.CENTRE);

		return cellFormat;
	}

	private static void fillDetailsInTheFirstColumn(String[] arrayFirstColumn,
			WritableSheet excelSheet) throws RowsExceededException,
			WriteException {

		int rowTDE = findTheTDERowNumber(arrayFirstColumn);

		for (int row = 0; row < arrayFirstColumn.length; row++) {
			if (row > 1 && row < (rowTDE)) {
				L = new Label(0, row, arrayFirstColumn[row], getCellFormat(
						Colour.VERY_LIGHT_YELLOW, Colour.BLACK));
				excelSheet.addCell(L);
			} else if (row > -1 && row < 2) {
				L = new Label(0, row, arrayFirstColumn[row], getCellFormat(
						Colour.AUTOMATIC, Colour.WHITE));
				excelSheet.addCell(L);
			} else if (row > rowTDE) {
				L = new Label(0, row, arrayFirstColumn[row], getCellFormat(
						Colour.GREEN, Colour.WHITE));
				excelSheet.addCell(L);
			} else if (row == rowTDE) {
				L = new Label(0, row, arrayFirstColumn[row], getCellFormat(
						Colour.AUTOMATIC, Colour.WHITE));
				excelSheet.addCell(L);
			}
		}
	}

	private static int findTheIncomeRowNumber(String[] arrayFirstColumn) {
		String checkData = "Income";
		int checkNum = 100;

		for (int arrayPosNum = 0; arrayPosNum < arrayFirstColumn.length; arrayPosNum++) {
			checkNum = checkData.compareTo(arrayFirstColumn[arrayPosNum]);
			if (checkNum == 0) {
				checkNum = arrayPosNum;
				arrayPosNum = arrayFirstColumn.length;
			}
		}
		return checkNum;
	}

	private static int getSpecifiedMonthsSalary() throws NumberFormatException,
			IOException {

		// Hard-coded Value
		int salary = 0;

		// methodForInsertingSeparatorToConsole();
		// System.out.println("Enter your income for the current month:");
		// int salary = Integer.parseInt(bufferRead.readLine());

		return salary;
	}

	private static int findTheTDERowNumber(String[] arrayFirstColumn) {
		String checkData = "TDE";
		int checkNum = 100;

		for (int arrayPosNum = 0; arrayPosNum < arrayFirstColumn.length; arrayPosNum++) {
			checkNum = checkData.compareTo(arrayFirstColumn[arrayPosNum]);
			if (checkNum == 0) {
				checkNum = arrayPosNum;
				arrayPosNum = arrayFirstColumn.length;
			}
		}
		return checkNum;
	}

	private static WritableCellFormat getCellFormat(Colour colourBack,
			Colour colourFont) throws WriteException {

		WritableFont fontFormat = new WritableFont(WritableFont.ARIAL, 10,
				WritableFont.NO_BOLD, false, UnderlineStyle.NO_UNDERLINE,
				colourFont);
		WritableCellFormat cellFormat = new WritableCellFormat(fontFormat);
		cellFormat.setBackground(colourBack);

		return cellFormat;
	}

	private static WritableSheet createANewSheetAtIndex0OftheSpecifiedWorkbook(
			String sheetName, WritableWorkbook wworkB) {

		wworkB.createSheet(sheetName, 0);
		System.out.println("Inserted the new sheet into the workbook.");
		WritableSheet excelSheet = wworkB.getSheet(0);
		return excelSheet;
	}

	private static String getSheetName(int year, int month) {

		String sheetName = new DateFormatSymbols().getMonths()[month] + " "
				+ year;
		return sheetName;
	}

	private static float getWeeksInTheSpecifiedMonth(int days, int year,
			int month) {

		float day = days;
		float weeks = day / 7;

		return weeks;
	}

	private static int getDaysInTheSpecifiedMonth(int year, int month) {

		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, 1);

		int days = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);

		return days;
	}

	private static WritableWorkbook createANewMonthlyExpenseWorkbook(int year)
			throws BiffException, IOException {
		WritableWorkbook wworkB = Workbook.createWorkbook(new File(
				getDestinationPath() + getDestnXLSFileName(year)));

		return wworkB;
	}

	private static WritableWorkbook addSheetsToAnExistingWorkbook(int year)
			throws BiffException, IOException {

		WritableWorkbook wworkB = Workbook.createWorkbook(new File(
				getDestinationPath() + getDestnXLSFileName(year)), Workbook
				.getWorkbook(new File(getSourcePath()
						+ getSourceXLSFileName(year))));

		return wworkB;
	}

	public static String getSourceXLSFileName(int year) throws IOException {

		// Hard-coded Value
		String sourceFileName = "Expenses " + year + ".xls";

		// Getting the file name from the console.
		// methodForInsertingSeparatorToConsole();
		// System.out.println("Enter the name of your source file:");
		// String sourceFileName = bufferRead.readLine() + ".xls";

		return sourceFileName;
	}

	private static String getSourcePath() throws IOException {
		// Hard-coded Value
		String sourcePath = "";

		// Getting the path from the console.
		// methodForInsertingSeparatorToConsole();
		// System.out.println("Enter the source directory where your file is:");
		// String sourcePath = bufferRead.readLine() + "\\";

		sourcePath = sourcePath.replace("\\", "/");

		return sourcePath;
	}

	private static int getYearFromConsole() throws NumberFormatException,
			IOException {

		// Hard-coded Value
		// int year = 2011;

		// Getting the month from the console.
		methodForInsertingSeparatorToConsole();
		System.out.println("Enter the year for the new sheet:");
		int year = Integer.parseInt(bufferRead.readLine());

		return year;
	}

	private static int getMonthFromConsole() throws NumberFormatException,
			IOException {

		// Hard-coded Value
		// int month = Calendar.FEBRUARY;

		// Getting the month from the console.
		methodForInsertingSeparatorToConsole();
		System.out
				.println("Enter the month (numerical value) for the new sheet:");
		int month = Integer.parseInt(bufferRead.readLine()) - 1;

		return month;
	}

	public static String getDestnXLSFileName(int year) throws IOException {

		// Hard-coded Value
		String destnFileName = className + " Expenses " + year + ".xls";

		// Getting the file name from the console.
		// methodForInsertingSeparatorToConsole();
		// System.out.println("Enter a name for your destination file:");
		// String destnFileName = bufferRead.readLine() + ".xls";

		return destnFileName;
	}

	private static String getDestinationPath() throws IOException {

		// Hard-coded Value
		String destnPath = "";

		// Getting the path from the console.
		// methodForInsertingSeparatorToConsole();
		// System.out
		// .println("Enter the destination directory where you wish to create your file:");
		// String destnPath = bufferRead.readLine() + "\\";

		destnPath = destnPath.replace("\\", "/");

		return destnPath;
	}

	public static void lineDivider() {
		System.out
				.println("--------------------------------------------------------------------------");

	}

	public static void methodForInsertingSeparatorToConsole() {

		System.out
				.println("**************************************************************************");

	}

}
