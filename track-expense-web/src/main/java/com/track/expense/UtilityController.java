package com.track.expense;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.track.expense.model.Expense;
import com.track.expense.model.ExtractData;
import com.track.expense.model.Income;
import com.track.expense.repository.ExpenseRepository;
import com.track.expense.repository.IncomeRepository;

@Controller
public class UtilityController {

	private final Logger logger = LoggerFactory.getLogger(UtilityController.class);

	final String navigation = "utility";

	@Autowired
	ExpenseRepository expenseRepo;

	@Autowired
	IncomeRepository incomeRepo;

	@RequestMapping(value = navigation)
	public ModelAndView defaultView(ModelAndView modelView) {
		logger.debug(">> defaultView()");

		modelView = constructModelView(modelView);

		logger.debug("<< defaultView()");
		return modelView;
	}

	@RequestMapping(value = "/extract-data")
	public ModelAndView extractData(ModelAndView modelView, ExtractData extractData, @RequestParam("upload-file") MultipartFile multipartFile) {
		logger.debug(">> extractData()");

		if (!multipartFile.isEmpty()) {

			switch (extractData.getBank()) {
			case "rbc":

				switch (extractData.getAccountType()) {

				case "chequing":

					com.track.expense.rbc.ChequingAccount ca = new com.track.expense.rbc.ChequingAccount();
					Map<String, Object> rbcChequeStore = ca.extract(multipartFile);

					@SuppressWarnings("unchecked")
					List<Expense> rbcChequelExpenses = (List<Expense>) rbcChequeStore.get("lExpenses");
					expenseRepo.save(rbcChequelExpenses);

					@SuppressWarnings("unchecked")
					List<Income> rbcChequelIncomes = (List<Income>) rbcChequeStore.get("lIncomes");
					incomeRepo.save(rbcChequelIncomes);

					break;

				case "credit-card":

					com.track.expense.rbc.CreditCard cc = new com.track.expense.rbc.CreditCard();
					Map<String, Object> rbcCreditStore = cc.extract(multipartFile);

					@SuppressWarnings("unchecked")
					List<Expense> rbcCreditlExpenses = (List<Expense>) rbcCreditStore.get("lExpenses");
					expenseRepo.save(rbcCreditlExpenses);

					@SuppressWarnings("unchecked")
					List<Income> rbcCreditlIncomes = (List<Income>) rbcCreditStore.get("lIncomes");
					incomeRepo.save(rbcCreditlIncomes);

					break;

				default:
					logger.debug("Work in progress for ... " + extractData.getAccountType());
					break;
				}

				break;

			default:
				logger.debug("Work in progress for ... " + extractData.getBank());
				break;
			}

		} else {
			logger.debug("No file uploaded");
		}

		modelView = constructModelView(modelView);

		logger.debug("<< extractData()");
		return modelView;
	}

	private ModelAndView constructModelView(ModelAndView modelView) {

		modelView.addObject("extractData", new ExtractData());

		Map<String, String> banks = new LinkedHashMap<String, String>();
		banks.put("rbc", "Royal Bank of Canada");
		banks.put("bmo", "Bank of Montreal");
		banks.put("td", "TD Canada Trust");
		banks.put("scotia", "Scotiabank");
		banks.put("cibc", "Canadian Imperial Bank of Commerce");
		modelView.addObject("banks", banks);

		Map<String, String> persons = new LinkedHashMap<String, String>();
		persons.put("rawoof", "Rawoof");
		persons.put("zubeda", "Zubeda");
		persons.put("mohammed", "Mohammed");
		persons.put("nadia", "Nadia");
		persons.put("nada", "Nada");
		modelView.addObject("persons", persons);

		Map<String, String> accountType = new LinkedHashMap<String, String>();
		accountType.put("chequing", "Chequing Account");
		accountType.put("saving", "Saving Account");
		accountType.put("credit-card", "Credit Card");
		modelView.addObject("accountType", accountType);

		modelView.setViewName(navigation);

		return modelView;
	}

}
