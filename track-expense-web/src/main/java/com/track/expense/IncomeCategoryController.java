package com.track.expense;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.track.expense.model.IncomeCategory;
import com.track.expense.repository.IncomeCategoryRepository;

@Controller
public class IncomeCategoryController {

	private final Logger _logger = LoggerFactory.getLogger(IncomeCategoryController.class);

	final String navigation = "income-category";

	@Autowired
	IncomeCategoryRepository incomeCategoryRepo;

	List<IncomeCategory> lIncomeCategory;

	@RequestMapping(value = navigation)
	public ModelAndView defaultView(ModelAndView modelView) {
		_logger.debug(">> defaultView()");

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< defaultView()");
		return modelView;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/save-income-category")
	public ModelAndView saveCategory(ModelAndView modelView, com.track.expense.form.IncomeCategory formCategory) {
		_logger.debug(">> saveCategory()");

		_logger.debug("-- saving: " + formCategory.getSaveCategoryId());

		IncomeCategory category = new IncomeCategory();
		category.setId(formCategory.getSaveCategoryId());
		category.setName(formCategory.getName());

		category = incomeCategoryRepo.save(category);
		incomeCategoryRepo.refreshCache();
		modelView.addObject("categorySaveSuccess", true);

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< saveCategory()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete-income-category")
	public ModelAndView deleteCategory(ModelAndView modelView, com.track.expense.form.IncomeCategory formCategory) {
		_logger.debug(">> deleteCategory()");

		_logger.debug("-- deleting: " + formCategory.getDeleteCategoryId());

		incomeCategoryRepo.delete(formCategory.getDeleteCategoryId());
		incomeCategoryRepo.refreshCache();
		modelView.addObject("categoryDeleteSuccess", true);

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< deleteCategory()");
		return modelView;
	}

	private ModelAndView constructDefaultModelView(ModelAndView modelView) {
		_logger.debug(">> constructModelView()");
		
		modelView.addObject("saveCategory", new com.track.expense.form.IncomeCategory());
		modelView.addObject("deleteCategory", new com.track.expense.form.IncomeCategory());

		lIncomeCategory = incomeCategoryRepo.findAll();
		modelView.addObject("lIncomeCategory", lIncomeCategory);

		modelView.addObject("navigation", navigation);
		modelView.setViewName(navigation);

		_logger.debug("<< constructModelView()");
		return modelView;
	}

}
