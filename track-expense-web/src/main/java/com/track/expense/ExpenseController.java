package com.track.expense;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.track.expense.chart.PieChartData;
import com.track.expense.form.SearchExpense;
import com.track.expense.model.Expense;
import com.track.expense.model.ExpenseCategory;
import com.track.expense.model.Person;
import com.track.expense.repository.ExpenseCategoryRepository;
import com.track.expense.repository.ExpenseRepository;
import com.track.expense.repository.PersonRepository;

@Controller
public class ExpenseController {

	private final Logger _logger = LoggerFactory.getLogger(ExpenseController.class);

	final String navigation = "expense";

	@Autowired
	PersonRepository personRepo;

	@Autowired
	ExpenseRepository expenseRepo;

	@Autowired
	ExpenseCategoryRepository expenseCategoryRepo;

	List<Person> lPerson;
	List<ExpenseCategory> lExpenseCategory;

	@RequestMapping(value = navigation)
	public ModelAndView defaultView(ModelAndView modelView) {
		_logger.debug(">> defaultView()");

		modelView = constructDefaultModelView(modelView);

		modelView = searchLast30Days(modelView);

		_logger.debug("<< defaultView()");
		return modelView;
	}

	@RequestMapping(value = "/search-expense")
	public ModelAndView searchExpense(ModelAndView modelView, SearchExpense searchExpense) {
		_logger.debug(">> searchExpense()");

		modelView = constructDefaultModelView(modelView);

		List<Expense> lExpenses = expenseRepo.searchExpenses(searchExpense);
		modelView.addObject("lExpenses", lExpenses);

		modelView.addObject("searchExpense", searchExpense);

		modelView = constructPieChartData(modelView, searchExpense);

		_logger.debug("<< searchExpense()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save-expense")
	public ModelAndView saveExpense(ModelAndView modelView, com.track.expense.form.Expense formExpense) {
		_logger.debug(">> saveExpense()");

		_logger.debug("-- saving: " + formExpense.getSaveExpenseId());

		Expense expense = new Expense();
		expense.setId(formExpense.getSaveExpenseId());
		expense.setDateTime(formExpense.getDateTime());
		expense.setDescription(formExpense.getDescription());
		expense.setAmount(formExpense.getAmount());
		expense.setExpenseCategory(formExpense.getExpenseCategory());
		expense.setPerson(formExpense.getPerson());

		expense = expenseRepo.save(expense);
		modelView.addObject("expenseSaveSuccess", true);

		modelView = constructDefaultModelView(modelView);

		modelView = searchLast30Days(modelView);

		_logger.debug("<< saveExpense()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete-expense")
	public ModelAndView deleteExpense(ModelAndView modelView, com.track.expense.form.Expense expense) {
		_logger.debug(">> deleteExpense()");

		_logger.debug("-- deleting: " + expense.getDeleteExpenseId());
		expenseRepo.delete(expense.getDeleteExpenseId());
		modelView.addObject("expenseDeleteSuccess", true);

		modelView = constructDefaultModelView(modelView);

		modelView = searchLast30Days(modelView);

		_logger.debug("<< deleteExpense()");
		return modelView;
	}

	private ModelAndView constructDefaultModelView(ModelAndView modelView) {
		_logger.debug(">> constructModelView()");

		modelView.addObject("saveExpense", new com.track.expense.form.Expense());
		modelView.addObject("deleteExpense", new com.track.expense.form.Expense());

		lPerson = personRepo.findAll();
		modelView.addObject("lPerson", lPerson);

		lExpenseCategory = expenseCategoryRepo.findAll();
		modelView.addObject("lExpenseCategory", lExpenseCategory);

		modelView.addObject("navigation", navigation);
		modelView.setViewName(navigation);

		_logger.debug("<< constructModelView()");
		return modelView;
	}

	private ModelAndView searchLast30Days(ModelAndView modelView) {
		_logger.debug(">> searchLast30Days()");

		List<Expense> lExpenses = expenseRepo.searchLast30Days();
		modelView.addObject("lExpenses", lExpenses);

		SearchExpense searchExpense = new SearchExpense();
		Calendar cal = Calendar.getInstance();
		searchExpense.setToDate(cal.getTime());
		cal.add(Calendar.MONTH, -1);
		searchExpense.setFromDate(cal.getTime());
		modelView.addObject("searchExpense", searchExpense);

		modelView = constructPieChartData(modelView, searchExpense);

		_logger.debug("<< searchLast30Days()");
		return modelView;
	}

	private ModelAndView constructPieChartData(ModelAndView modelView, SearchExpense searchExpense) {
		_logger.debug(">> constructPieChartData()");

		List<PieChartData> lExpensesByCategory = buildChartDataByExpenseCategory(searchExpense);
		modelView.addObject("lExpensesByCategory", lExpensesByCategory);
		try {
			modelView.addObject("jsonDataByCategory", new ObjectMapper().writeValueAsString(lExpensesByCategory));
		} catch (JsonProcessingException e) {
			_logger.error("-- " + e.getMessage());
		}

		List<PieChartData> lExpensesByPerson = buildChartDataByPerson(searchExpense);
		modelView.addObject("lExpensesByPerson", lExpensesByPerson);
		try {
			modelView.addObject("jsonDataByPerson", new ObjectMapper().writeValueAsString(lExpensesByPerson));
		} catch (JsonProcessingException e) {
			_logger.error("-- " + e.getMessage());
		}

		_logger.debug("<< constructPieChartData()");
		return modelView;
	}

	private List<PieChartData> buildChartDataByExpenseCategory(SearchExpense searchExpense) {
		_logger.debug(">> buildChartDataByExpenseCategory()");

		int i = 0;
		List<PieChartData> lExpensesByCategory = new ArrayList<PieChartData>();
		for (ExpenseCategory expenseCategory : lExpenseCategory) {
			PieChartData p = new PieChartData();

			p.setColor(Namespace.colors[i][0]);
			p.setHighlight(Namespace.colors[i][1]);
			p.setLabel(expenseCategory.getName());

			Double amount = 0D;
			for (Expense e : expenseRepo.getByExpenseCategory(expenseCategory.getId(), searchExpense.getFromDate(), searchExpense.getToDate())) {
				amount = e.getAmount() + amount;
			}
			p.setValue(Precision.round(amount, 2));

			lExpensesByCategory.add(p);

			i++;
		}

		try {
			_logger.debug("-- " + new ObjectMapper().writeValueAsString(lExpensesByCategory));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		_logger.debug("<< buildChartDataByExpenseCategory()");
		return lExpensesByCategory;
	}

	private List<PieChartData> buildChartDataByPerson(SearchExpense searchExpense) {
		_logger.debug(">> buildChartDataByPerson()");

		int i = 0;
		List<PieChartData> lExpensesByPerson = new ArrayList<PieChartData>();
		for (Person person : lPerson) {
			PieChartData p = new PieChartData();

			p.setColor(Namespace.colors[i][0]);
			p.setHighlight(Namespace.colors[i][1]);
			p.setLabel(person.getName());

			Double amount = 0D;
			for (Expense e : expenseRepo.getByPerson(person.getId(), searchExpense.getFromDate(), searchExpense.getToDate())) {
				amount = e.getAmount() + amount;
			}
			p.setValue(Precision.round(amount, 2));

			lExpensesByPerson.add(p);

			i++;
		}

		try {
			_logger.debug("-- " + new ObjectMapper().writeValueAsString(lExpensesByPerson));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		_logger.debug("<< buildChartDataByPerson()");
		return lExpensesByPerson;
	}

}
