package com.track.expense.repository.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.track.expense.form.SearchExpense;
import com.track.expense.model.Expense;
import com.track.expense.model.ExpenseCategory;
import com.track.expense.model.Person;
import com.track.expense.repository.ExpenseCategoryRepository;
import com.track.expense.repository.ExpenseRepositoryCustom;
import com.track.expense.repository.PersonRepository;

public class ExpenseRepositoryImpl implements ExpenseRepositoryCustom {

	private final Logger _logger = LoggerFactory.getLogger(ExpenseRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	PersonRepository personRepo;

	@Autowired
	ExpenseCategoryRepository expenseCategoryRepo;

	@Override
	public List<Expense> searchExpenses(SearchExpense searchExpense) {
		_logger.debug(">> searchExpenses()");

		String log = "";
		DateFormat logDf = new SimpleDateFormat("dd-MMM-yyyy");

		// Build search query
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Expense> cq = cb.createQuery(Expense.class);
		Root<Expense> r = cq.from(Expense.class);
		List<Predicate> lPredicates = new ArrayList<Predicate>();

		// Search condition - transaction date
		if (searchExpense.getFromDate() != null && searchExpense.getToDate() != null) {
			log += "transaction-date-range: from - " + logDf.format(searchExpense.getFromDate()) + " to - " + logDf.format(searchExpense.getToDate());
			lPredicates.add(cb.between(r.<Date>get("dateTime"), searchExpense.getFromDate(), searchExpense.getToDate()));
		}

		// Search condition - person-id
		try {
			if (searchExpense.getSearchPerson() != null && searchExpense.getSearchPerson().getId() > 0) {
				Person person = personRepo.findOne(searchExpense.getSearchPerson().getId());
				log += ", person - " + person.getName();
				lPredicates.add(cb.equal(r.get("person"), person));
			}
		} catch (Exception e) {
			_logger.error("-- " + e.getMessage());
		}

		// Search condition - expense category
		try {
			if (searchExpense.getSearchExpenseCategory() != null && searchExpense.getSearchExpenseCategory().getId() > 0) {
				ExpenseCategory expCateg = expenseCategoryRepo.findOne(searchExpense.getSearchExpenseCategory().getId());
				log += ", category - " + expCateg.getName();
				lPredicates.add(cb.equal(r.get("expenseCategory"), expCateg));
			}
		} catch (Exception e) {
			_logger.error("-- " + e.getMessage());
		}

		// Logging search parameters
		_logger.debug("-- " + log);

		// Perform query and get result
		cq.select(r);
		cq.where(cb.and(lPredicates.toArray(new Predicate[] {})));

		List<Expense> lExpenses = em.createQuery(cq).getResultList();
		_logger.debug("-- size: " + lExpenses.size());

		_logger.debug("<< searchExpenses()");
		return lExpenses;
	}

	@Override
	public List<Expense> searchLast30Days() {
		_logger.debug(">> searchLast30Days()");

		SearchExpense searchExpense = new SearchExpense();
		Calendar cal = Calendar.getInstance();
		searchExpense.setToDate(cal.getTime());
		cal.add(Calendar.MONTH, -1);
		searchExpense.setFromDate(cal.getTime());

		List<Expense> lExpenses = searchExpenses(searchExpense);

		_logger.debug("<< searchLast30Days()");
		return lExpenses;
	}

}
