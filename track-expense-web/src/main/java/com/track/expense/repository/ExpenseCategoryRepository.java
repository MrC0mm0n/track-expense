package com.track.expense.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.track.expense.model.ExpenseCategory;

@Repository
public interface ExpenseCategoryRepository extends JpaRepository<ExpenseCategory, Integer>, ExpenseCategoryRepositoryCustom {

	@Override
	@Cacheable(value = "expense-category", key = "'find-all'")
	public List<ExpenseCategory> findAll();
	
}
