package com.track.expense.repository.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.track.expense.form.SearchIncome;
import com.track.expense.model.Income;
import com.track.expense.model.IncomeCategory;
import com.track.expense.model.Person;
import com.track.expense.repository.IncomeCategoryRepository;
import com.track.expense.repository.IncomeRepositoryCustom;
import com.track.expense.repository.PersonRepository;

public class IncomeRepositoryImpl implements IncomeRepositoryCustom {

	private final Logger _logger = LoggerFactory.getLogger(ExpenseRepositoryImpl.class);

	@PersistenceContext
	private EntityManager em;

	@Autowired
	PersonRepository personRepo;

	@Autowired
	IncomeCategoryRepository incomeCategoryRepo;

	@Override
	public List<Income> searchIncomes(SearchIncome searchIncome) {
		_logger.debug(">> searchIncomes()");

		String log = "";
		DateFormat logDf = new SimpleDateFormat("dd-MMM-yyyy");

		// Build search query
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Income> cq = cb.createQuery(Income.class);
		Root<Income> r = cq.from(Income.class);
		List<Predicate> lPredicates = new ArrayList<Predicate>();

		// Search condition - transaction date
		if (searchIncome.getFromDate() != null && searchIncome.getToDate() != null) {
			log += "transaction-date-range: from - " + logDf.format(searchIncome.getFromDate()) + " to - " + logDf.format(searchIncome.getToDate());
			lPredicates.add(cb.between(r.<Date>get("dateTime"), searchIncome.getFromDate(), searchIncome.getToDate()));
		}

		// Search condition - person-id
		try {
			if (searchIncome.getSearchPerson() != null && searchIncome.getSearchPerson().getId() > 0) {
				Person person = personRepo.findOne(searchIncome.getSearchPerson().getId());
				log += ", person - " + person.getName();
				lPredicates.add(cb.equal(r.get("person"), person));
			}
		} catch (Exception e) {
			_logger.error("-- " + e.getMessage());
		}

		// Search condition - income category
		try {
			if (searchIncome.getSearchIncomeCategory() != null && searchIncome.getSearchIncomeCategory().getId() > 0) {
				IncomeCategory expCateg = incomeCategoryRepo.findOne(searchIncome.getSearchIncomeCategory().getId());
				log += ", category - " + expCateg.getName();
				lPredicates.add(cb.equal(r.get("incomeCategory"), expCateg));
			}
		} catch (Exception e) {
			_logger.error("-- " + e.getMessage());
		}

		// Logging search parameters
		_logger.debug("-- " + log);

		// Perform query and get result
		cq.select(r);
		cq.where(cb.and(lPredicates.toArray(new Predicate[] {})));

		List<Income> lIncomes = em.createQuery(cq).getResultList();
		_logger.debug("-- size: " + lIncomes.size());

		_logger.debug("<< searchIncomes()");
		return lIncomes;
	}

	@Override
	public List<Income> searchLast30Days() {
		_logger.debug(">> searchLast30Days()");

		SearchIncome searchIncome = new SearchIncome();
		Calendar cal = Calendar.getInstance();
		searchIncome.setToDate(cal.getTime());
		cal.add(Calendar.MONTH, -1);
		searchIncome.setFromDate(cal.getTime());

		List<Income> lIncomes = searchIncomes(searchIncome);

		_logger.debug("<< searchLast30Days()");
		return lIncomes;
	}

}
