package com.track.expense.repository;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

@Repository
public interface IncomeCategoryRepositoryCustom {

	@CacheEvict(value = "income-category", allEntries = true)
	public void refreshCache();

}
