package com.track.expense.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.track.expense.repository.IncomeCategoryRepositoryCustom;

public class IncomeCategoryRepositoryImpl implements IncomeCategoryRepositoryCustom {

	private final Logger _logger = LoggerFactory.getLogger(IncomeCategoryRepositoryImpl.class);

	@Override
	public void refreshCache() {
		_logger.debug(">> refreshCache()");

		_logger.debug("<< refreshCache()");
	}
	
}
