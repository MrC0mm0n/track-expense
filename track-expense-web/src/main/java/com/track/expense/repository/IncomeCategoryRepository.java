package com.track.expense.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.track.expense.model.IncomeCategory;

@Repository
public interface IncomeCategoryRepository extends JpaRepository<IncomeCategory, Integer>, IncomeCategoryRepositoryCustom {

	@Override
	@Cacheable(value = "income-category", key = "'find-all'")
	public List<IncomeCategory> findAll();

}
