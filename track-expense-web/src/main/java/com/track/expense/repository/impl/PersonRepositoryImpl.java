package com.track.expense.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.track.expense.repository.PersonRepositoryCustom;

public class PersonRepositoryImpl implements PersonRepositoryCustom {
	
	private final Logger _logger = LoggerFactory.getLogger(PersonRepositoryImpl.class);

	@Override
	public void refreshCache() {
		_logger.debug(">> refreshCache()");

		_logger.debug("<< refreshCache()");
	}
	
}
