package com.track.expense.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.track.expense.form.SearchIncome;
import com.track.expense.model.Income;

@Repository
public interface IncomeRepositoryCustom {

	List<Income> searchIncomes(SearchIncome searchIncome);

	List<Income> searchLast30Days();

}
