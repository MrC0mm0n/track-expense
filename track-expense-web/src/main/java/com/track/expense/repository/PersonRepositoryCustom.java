package com.track.expense.repository;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Repository;

@Repository
public interface PersonRepositoryCustom {

	@CacheEvict(value = "person", allEntries = true)
	public void refreshCache();

}
