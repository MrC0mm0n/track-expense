package com.track.expense.repository.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.track.expense.repository.ExpenseCategoryRepositoryCustom;

public class ExpenseCategoryRepositoryImpl implements ExpenseCategoryRepositoryCustom {

	private final Logger _logger = LoggerFactory.getLogger(ExpenseCategoryRepositoryImpl.class);

	@Override
	public void refreshCache() {
		_logger.debug(">> refreshCache()");

		_logger.debug("<< refreshCache()");
	}

}
