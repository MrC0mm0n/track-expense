package com.track.expense.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.track.expense.model.Loan;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Integer> {
	
	@Query(nativeQuery = true, value = "SELECT * FROM loan WHERE person_id=:personId")
	List<Loan> getByPerson(@Param("personId") Integer personId);

}
