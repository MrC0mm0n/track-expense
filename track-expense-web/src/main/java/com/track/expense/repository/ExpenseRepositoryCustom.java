package com.track.expense.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.track.expense.form.SearchExpense;
import com.track.expense.model.Expense;

@Repository
public interface ExpenseRepositoryCustom {

	List<Expense> searchExpenses(SearchExpense searchExpense);
	
	List<Expense> searchLast30Days();

}
