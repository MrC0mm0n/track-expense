package com.track.expense.repository;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.track.expense.model.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Integer>, PersonRepositoryCustom {

	@Query(nativeQuery = true, value = "SELECT " + "(SELECT ROUND(SUM(amount), 2) FROM income WHERE MONTH(DATETIME)=:month AND YEAR(DATETIME)=:year AND person_id=:personId)" + "-"
			+ "(SELECT ROUND(SUM(amount), 2) FROM expense WHERE MONTH(DATETIME)=:month AND YEAR(DATETIME)=:year AND person_id=:personId)")
	Double getIncomeMinusExpenseByPeriodAndPerson(@Param("month") Integer month, @Param("year") Integer year, @Param("personId") Integer personId);

	@Override
	@Cacheable(value = "person", key = "'find-all'")
	public List<Person> findAll();

}
