package com.track.expense.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.track.expense.model.Expense;

@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Long>, ExpenseRepositoryCustom {

	@Query(nativeQuery = true, value = "SELECT * FROM expense WHERE expenseCategory_id=:categoryId AND dateTime BETWEEN :fromDate AND :toDate")
	List<Expense> getByExpenseCategory(@Param("categoryId") Integer categoryId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query(nativeQuery = true, value = "SELECT * FROM expense WHERE person_id=:personId AND dateTime BETWEEN :fromDate AND :toDate")
	List<Expense> getByPerson(@Param("personId") Integer personId, @Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

	@Query(nativeQuery = true, value = "SELECT ROUND(SUM(amount), 2) FROM expense WHERE MONTH(DATETIME)=:month AND YEAR(DATETIME)=:year AND person_id=:personId AND expenseCategory_id=:categoryId")
	Double getSumByPeriodAndPersonAndCategory(@Param("month") Integer month, @Param("year") Integer year, @Param("personId") Integer personId, @Param("categoryId") Integer categoryId);

}