package com.track.expense.form;

import java.util.Date;

import com.track.expense.model.Expense;
import com.track.expense.model.ExpenseCategory;
import com.track.expense.model.Person;

public class SearchExpense extends Expense {

	Date fromDate;

	Date toDate;

	ExpenseCategory searchExpenseCategory;

	Person searchPerson;

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public ExpenseCategory getSearchExpenseCategory() {
		return searchExpenseCategory;
	}

	public void setSearchExpenseCategory(ExpenseCategory searchExpenseCategory) {
		this.searchExpenseCategory = searchExpenseCategory;
	}

	public Person getSearchPerson() {
		return searchPerson;
	}

	public void setSearchPerson(Person searchPerson) {
		this.searchPerson = searchPerson;
	}

}
