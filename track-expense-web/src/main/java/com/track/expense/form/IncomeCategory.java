package com.track.expense.form;

public class IncomeCategory extends com.track.expense.model.IncomeCategory {

	Integer saveCategoryId;

	Integer deleteCategoryId;

	public Integer getSaveCategoryId() {
		return saveCategoryId;
	}

	public void setSaveCategoryId(Integer saveCategoryId) {
		this.saveCategoryId = saveCategoryId;
	}

	public Integer getDeleteCategoryId() {
		return deleteCategoryId;
	}

	public void setDeleteCategoryId(Integer deleteCategoryId) {
		this.deleteCategoryId = deleteCategoryId;
	}

}
