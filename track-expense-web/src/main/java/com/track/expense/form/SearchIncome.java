package com.track.expense.form;

import java.util.Date;

import com.track.expense.model.IncomeCategory;
import com.track.expense.model.Person;

public class SearchIncome extends Income {

	Date fromDate;

	Date toDate;

	IncomeCategory searchIncomeCategory;

	Person searchPerson;

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public IncomeCategory getSearchIncomeCategory() {
		return searchIncomeCategory;
	}

	public void setSearchIncomeCategory(IncomeCategory searchIncomeCategory) {
		this.searchIncomeCategory = searchIncomeCategory;
	}

	public Person getSearchPerson() {
		return searchPerson;
	}

	public void setSearchPerson(Person searchPerson) {
		this.searchPerson = searchPerson;
	}

}
