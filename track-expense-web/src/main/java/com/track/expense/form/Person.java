package com.track.expense.form;

public class Person extends com.track.expense.model.Person {

	Integer savePersonId;

	Integer deletePersonId;

	public Integer getSavePersonId() {
		return savePersonId;
	}

	public void setSavePersonId(Integer savePersonId) {
		this.savePersonId = savePersonId;
	}

	public Integer getDeletePersonId() {
		return deletePersonId;
	}

	public void setDeletePersonId(Integer deletePersonId) {
		this.deletePersonId = deletePersonId;
	}

}
