package com.track.expense.form;

public class Loan extends com.track.expense.model.Loan {

	Integer saveLoanId;

	Integer deleteLoanId;

	public Integer getSaveLoanId() {
		return saveLoanId;
	}

	public void setSaveLoanId(Integer saveLoanId) {
		this.saveLoanId = saveLoanId;
	}

	public Integer getDeleteLoanId() {
		return deleteLoanId;
	}

	public void setDeleteLoanId(Integer deleteLoanId) {
		this.deleteLoanId = deleteLoanId;
	}

}
