package com.track.expense.form;

public class Income extends com.track.expense.model.Income {

	Long saveIncomeId;

	Long deleteIncomeId;

	public Long getSaveIncomeId() {
		return saveIncomeId;
	}

	public void setSaveIncomeId(Long saveIncomeId) {
		this.saveIncomeId = saveIncomeId;
	}

	public Long getDeleteIncomeId() {
		return deleteIncomeId;
	}

	public void setDeleteIncomeId(Long deleteIncomeId) {
		this.deleteIncomeId = deleteIncomeId;
	}

}
