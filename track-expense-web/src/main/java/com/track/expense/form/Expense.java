package com.track.expense.form;

public class Expense extends com.track.expense.model.Expense {

	Long saveExpenseId;

	Long deleteExpenseId;

	public Long getSaveExpenseId() {
		return saveExpenseId;
	}

	public void setSaveExpenseId(Long saveExpenseId) {
		this.saveExpenseId = saveExpenseId;
	}

	public Long getDeleteExpenseId() {
		return deleteExpenseId;
	}

	public void setDeleteExpenseId(Long deleteExpenseId) {
		this.deleteExpenseId = deleteExpenseId;
	}

}
