package com.track.expense;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.track.expense.model.Person;
import com.track.expense.repository.PersonRepository;

@Controller
public class PersonController {

	private final Logger _logger = LoggerFactory.getLogger(PersonController.class);

	final String navigation = "members";

	@Autowired
	PersonRepository personRepo;

	List<Person> lPerson;

	@RequestMapping(value = navigation)
	public ModelAndView defaultView(ModelAndView modelView) {
		_logger.debug(">> defaultView()");

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< defaultView()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save-member")
	public ModelAndView savePerson(ModelAndView modelView, com.track.expense.form.Person formPerson) {
		_logger.debug(">> savePerson()");

		_logger.debug("-- saving: " + formPerson.getSavePersonId());

		Person person = new Person();
		person.setId(formPerson.getSavePersonId());
		person.setName(formPerson.getName());
		person.setEmail(formPerson.getEmail());

		person = personRepo.save(person);
		personRepo.refreshCache();

		modelView.addObject("personSaveSuccess", true);

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< savePerson()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete-member")
	public ModelAndView deletePerson(ModelAndView modelView, com.track.expense.form.Person person) {
		_logger.debug(">> deletePerson()");

		_logger.debug("-- deleting: " + person.getDeletePersonId());

		personRepo.delete(person.getDeletePersonId());
		personRepo.refreshCache();

		modelView.addObject("personDeleteSuccess", true);

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< deletePerson()");
		return modelView;
	}

	private ModelAndView constructDefaultModelView(ModelAndView modelView) {
		_logger.debug(">> constructModelView()");

		modelView.addObject("savePerson", new com.track.expense.form.Person());
		modelView.addObject("deletePerson", new com.track.expense.form.Person());

		lPerson = personRepo.findAll();
		modelView.addObject("lPerson", lPerson);

		modelView.addObject("navigation", navigation);
		modelView.setViewName(navigation);

		_logger.debug("<< constructModelView()");
		return modelView;
	}

}
