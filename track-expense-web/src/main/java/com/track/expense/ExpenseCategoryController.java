package com.track.expense;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.track.expense.model.ExpenseCategory;
import com.track.expense.repository.ExpenseCategoryRepository;

@Controller
public class ExpenseCategoryController {

	private final Logger _logger = LoggerFactory.getLogger(ExpenseCategoryController.class);

	final String navigation = "expense-category";

	@Autowired
	ExpenseCategoryRepository expenseCategoryRepo;

	List<ExpenseCategory> lExpenseCategory;

	@RequestMapping(value = navigation)
	public ModelAndView defaultView(ModelAndView modelView) {
		_logger.debug(">> defaultView()");

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< defaultView()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save-expense-category")
	public ModelAndView saveCategory(ModelAndView modelView, com.track.expense.form.ExpenseCategory formCategory) {
		_logger.debug(">> saveCategory()");

		_logger.debug("-- saving: " + formCategory.getSaveCategoryId());

		ExpenseCategory category = new ExpenseCategory();
		category.setId(formCategory.getSaveCategoryId());
		category.setName(formCategory.getName());

		category = expenseCategoryRepo.save(category);
		expenseCategoryRepo.refreshCache();
		modelView.addObject("categorySaveSuccess", true);

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< saveCategory()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete-expense-category")
	public ModelAndView deleteCategory(ModelAndView modelView, com.track.expense.form.ExpenseCategory formCategory) {
		_logger.debug(">> deleteCategory()");

		_logger.debug("-- deleting: " + formCategory.getDeleteCategoryId());

		expenseCategoryRepo.delete(formCategory.getDeleteCategoryId());
		expenseCategoryRepo.refreshCache();
		modelView.addObject("categoryDeleteSuccess", true);

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< deleteCategory()");
		return modelView;
	}

	private ModelAndView constructDefaultModelView(ModelAndView modelView) {
		_logger.debug(">> constructModelView()");

		modelView.addObject("saveCategory", new com.track.expense.form.ExpenseCategory());
		modelView.addObject("deleteCategory", new com.track.expense.form.ExpenseCategory());

		lExpenseCategory = expenseCategoryRepo.findAll();
		modelView.addObject("lExpenseCategory", lExpenseCategory);

		modelView.addObject("navigation", navigation);
		modelView.setViewName(navigation);

		_logger.debug("<< constructModelView()");
		return modelView;
	}

}
