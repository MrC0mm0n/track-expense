package com.track.expense.chart;

public class Data {

	String[] labels;

	Datasets[] datasets;

	public Data() {
		datasets = new Datasets[1];
	}

	public String[] getLabels() {
		return labels;
	}

	public void setLabels(String[] labels) {
		this.labels = labels;
	}

	public Datasets[] getDatasets() {
		return datasets;
	}

	public void setDatasets(Datasets[] datasets) {
		this.datasets = datasets;
	}

}