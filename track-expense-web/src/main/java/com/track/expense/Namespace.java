package com.track.expense;

public class Namespace {

	// color, highlight
	public static String[][] colors = { { "#ea6485", "#ea6485" }, { "#3cb44b", "#8ee299" }, { "#ffe119", "#eddd71" }, { "#4363d8", "#7d94e8" }, { "#f58231", "#f7a871" }, { "#911eb4", "#be6fd6" },
			{ "#46f0f0", "#95f4f4" }, { "#f032e6", "#ed9ce8" }, { "#bcf60c", "#d1ea85" }, { "#f99595", "#fabebe" }, { "#008080", "#2bc6c6" }, { "#e6beff", "#ead9f4" }, { "#9A6324", "#c69863" },
			{ "#fffac8", "#e8e5cc" }, { "#800000", "#a34646" }, { "#aaffc3", "#dbf9e4" }, { "#808000", "#a0a026" }, { "#ffd8b1", "#f7e7d7" }, { "#000075", "#4e4e93" }, { "#e0d428", "#ede682" },
			{ "#5f486e", "#9d84ae" }, { "#d4f692", "#94d510" } };

}
