package com.track.expense.rbc;

import java.awt.Rectangle;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import com.track.expense.model.Expense;
import com.track.expense.model.Income;
import com.track.expense.repository.ExpenseRepository;
import com.track.expense.repository.IncomeRepository;

@Controller
public class CreditCard {

	private final Logger logger = LoggerFactory.getLogger(CreditCard.class);

	List<Expense> lExpenses;
	List<Income> lIncomes;

	int year = 0;

	@Autowired
	ExpenseRepository expenseRepo;

	@Autowired
	IncomeRepository incomeRepo;

	public Map<String, Object> extract(MultipartFile multipartFile) {
		logger.info(">> extract()");

		PDDocument document = null;
		try {
			// Setup
			document = PDDocument.load(multipartFile.getInputStream());

			int numOfPages = document.getNumberOfPages();

			lExpenses = new ArrayList<Expense>();
			lIncomes = new ArrayList<Income>();

			for (int i = 0; i < numOfPages; i++) {

				switch (i) {
				case 0:
					extractDetailsFromFirstPage(document.getPage(i));
					break;

				default:

					if (i > 0 && i < numOfPages) {
						extractDetailsFromMiddlePages(document.getPage(i));
					}

					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");

		logger.info("--- Expense ---");
		for (Expense expense : lExpenses) {
			logger.info(
					sdf.format(expense.getDateTime()) + ", " + expense.getDescription() + ", " + expense.getAmount());
		}

		logger.info("--- Income ---");
		for (Income income : lIncomes) {
			logger.info(sdf.format(income.getDateTime()) + ", " + income.getDescription() + ", " + income.getAmount());
		}

		Map<String, Object> store = new LinkedHashMap<String, Object>();
		store.put("lExpenses", lExpenses);
		store.put("lIncomes", lIncomes);

		logger.info("<< extract()");
		return store;
	}

	private void extractDetailsFromFirstPage(PDPage firstPage) throws IOException, ParseException {
		logger.info(">> extractDetailsFromFirstPage()");

		/**
		 * Setting up and extracting regions in PDF
		 */
		PDFTextStripperByArea stripper = new PDFTextStripperByArea();
		stripper.setSortByPosition(true);

		// Statement period
		stripper.addRegion("stmtPeriod", new Rectangle(10, 110, 350, 10));
		// Transaction details
		stripper.addRegion("page1", new Rectangle(10, 210, 350, 350));
		stripper.extractRegions(firstPage);

		String stmtPeriod = stripper.getTextForRegion("stmtPeriod");
		// System.out.println(stmtPeriod);
		year = Integer.parseInt(stmtPeriod.substring(stmtPeriod.indexOf(",") + 2).trim());

		// Process Transaction details
		String[] sArrData = stripper.getTextForRegion("page1").split("\n");
		extractExpense(sArrData);

		logger.info("<< extractDetailsFromFirstPage()");
	}

	private void extractDetailsFromMiddlePages(PDPage pdPage) throws IOException, ParseException {
		logger.info(">> extractDetailsFromMiddlePages()");

		PDFTextStripperByArea stripper = new PDFTextStripperByArea();
		stripper.setSortByPosition(true);
		stripper.addRegion("middlePage", new Rectangle(10, 200, 350, 700));
		stripper.extractRegions(pdPage);

		String[] sArrData = stripper.getTextForRegion("middlePage").split("\n");
		extractExpense(sArrData);

		logger.info("<< extractDetailsFromMiddlePages()");
	}

	private void extractExpense(String[] sArrData) throws IOException, ParseException {

		for (int i = 0; i < sArrData.length; i++) {
			if (filterCriteria(sArrData[i])) {

				String sDate = sArrData[i].substring(0, 6);
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
				Date date = sdf.parse(sDate.split(" ")[1] + "/" + sDate.split(" ")[0] + "/" + year);

				String desc = sArrData[i].substring(14, sArrData[i].indexOf("$")).trim();

				double amt = Double.valueOf(sArrData[i].substring(sArrData[i].indexOf("$") - 1, sArrData[i].length())
						.trim().replaceAll(",", "").replaceAll("\\$", ""));

				if (amt > 0) {

					Expense expense = new Expense();

					expense.setDateTime(date);
					expense.setDescription(desc);
					expense.setAmount(amt);

					lExpenses.add(expense);

				} else {

					Income income = new Income();

					income.setDateTime(date);
					income.setDescription(desc.substring(0, desc.length() - 2));
					income.setAmount(amt * -1);

					lIncomes.add(income);

				}

			}
		}
	}

	private boolean filterCriteria(String data) {
		boolean check = false;

		if ((data.startsWith("JAN") || data.startsWith("FEB") || data.startsWith("MAR") || data.startsWith("APR")
				|| data.startsWith("MAY") || data.startsWith("JUN") || data.startsWith("JUL") || data.startsWith("AUG")
				|| data.startsWith("SEP") || data.startsWith("OCT") || data.startsWith("NOV") || data.startsWith("DEC"))
				&& (!data.contains("PAYMENT - THANK YOU"))) {
			check = true;
		}

		return check;
	}

}
