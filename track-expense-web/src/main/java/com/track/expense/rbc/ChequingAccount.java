package com.track.expense.rbc;

import java.awt.Rectangle;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

import com.track.expense.model.Expense;
import com.track.expense.model.Income;

public class ChequingAccount {

	private final Logger logger = LoggerFactory.getLogger(ChequingAccount.class);

	List<Expense> lExpenses;
	List<Income> lIncomes;
	List<String> lData;

	int year = 0;

	public Map<String, Object> extract(MultipartFile multipartFile) {
		logger.info(">> extract()");

		// Setup
		PDDocument document = null;
		try {
			document = PDDocument.load(multipartFile.getInputStream());

			int numOfPages = document.getNumberOfPages();

			lExpenses = new ArrayList<Expense>();
			lIncomes = new ArrayList<Income>();
			lData = new ArrayList<String>();

			for (int i = 0; i < numOfPages; i++) {

				switch (i) {
				case 0:
					extractDetailsFromFirstPage(document.getPage(i));
					break;

				default:

					if (i > 0 && i < numOfPages) {
						extractDetailsFromMiddlePages(document.getPage(i));
					}

					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (document != null) {
				try {
					document.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		extractExpense();

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");

		logger.debug("--- Expense ---");
		for (Expense expense : lExpenses) {
			logger.debug(
					sdf.format(expense.getDateTime()) + ", " + expense.getDescription() + ", " + expense.getAmount());
		}

		logger.debug("--- Income ---");
		for (Income income : lIncomes) {
			logger.debug(sdf.format(income.getDateTime()) + ", " + income.getDescription() + ", " + income.getAmount());
		}

		Map<String, Object> store = new LinkedHashMap<String, Object>();
		store.put("lExpenses", lExpenses);
		store.put("lIncomes", lIncomes);

		logger.info("<< extract()");
		return store;
	}

	private void extractDetailsFromFirstPage(PDPage firstPage) throws IOException, ParseException {
		logger.debug(">> extractDetailsFromFirstPage()");

		/**
		 * Setting up and extracting regions in PDF
		 */
		PDFTextStripperByArea stripper = new PDFTextStripperByArea();
		stripper.setSortByPosition(true);

		// Statement period
		stripper.addRegion("stmtPeriod", new Rectangle(400, 100, 200, 40));
		// Transaction details
		stripper.addRegion("page1", new Rectangle(30, 480, 500, 250));
		stripper.extractRegions(firstPage);

		String stmtPeriod = stripper.getTextForRegion("stmtPeriod");
		// System.out.println(stmtPeriod);
		year = Integer.parseInt(stmtPeriod.substring(stmtPeriod.indexOf(",") + 1, stmtPeriod.indexOf("to")).trim());
		// System.out.println("year: " + year);

		// Process Transaction details
		// System.out.println(stripper.getTextForRegion("page1"));
		String[] sArrData = stripper.getTextForRegion("page1").split("\n");

		for (int i = 0; i < sArrData.length; i++) {
			lData.add(sArrData[i]);
		}

		logger.debug("<< extractDetailsFromFirstPage()");

	}

	private void extractDetailsFromMiddlePages(PDPage pdPage) throws IOException, ParseException {
		logger.debug(">> extractDetailsFromMiddlePages()");

		PDFTextStripperByArea stripper = new PDFTextStripperByArea();
		stripper.setSortByPosition(true);
		stripper.addRegion("middlePage", new Rectangle(10, 160, 450, 700));
		stripper.extractRegions(pdPage);

		// System.out.println(stripper.getTextForRegion("middlePage"));
		String[] sArrData = stripper.getTextForRegion("middlePage").split("\n");
		int stopAt = 0;
		for (int i = 0; i < sArrData.length; i++) {
			if (sArrData[i].contains("Closing Balance")) {
				stopAt = i;
			}
		}

		// System.out.println("stopAt: " + stopAt);
		for (int i = 0; i < stopAt; i++) {
			lData.add(sArrData[i]);
		}

		logger.debug("<< extractDetailsFromMiddlePages()");
	}

	private void extractExpense() {

		Date date = null;
		String desc = "";
		double amt = 0;

		for (String data : lData) {
			amt = 0;

			String sDate = data.substring(0, 6);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");

			try {
				date = sdf.parse(sDate.split(" ")[0] + "/" + sDate.split(" ")[1] + "/" + year);
				desc = data.substring(6, data.lastIndexOf(" ")).trim();
			} catch (Exception e) {
				// e.printStackTrace();
				// System.out.println("- " + data);
				desc = data.substring(0, data.lastIndexOf(" ")).trim();
			}

			try {
				amt = Double.valueOf(data.substring(data.lastIndexOf(" "), data.length()).trim().replaceAll(",", "")
						.replaceAll("\\$", ""));
			} catch (Exception e) {
				// e.printStackTrace();
				// System.out.println("- " + data);
				amt = 0;
				desc = data.substring(6);
			}

			// System.out.println(sdf.format(date) + ", " + desc + ", " + amt);

			if (isIncome(desc)) {

				Income income = new Income();

				income.setDateTime(date);
				income.setDescription(desc);
				income.setAmount(amt);

				lIncomes.add(income);

			} else {

				Expense expense = new Expense();

				expense.setDateTime(date);
				expense.setDescription(desc);
				expense.setAmount(amt);

				lExpenses.add(expense);

			}

		}

	}

	private boolean isIncome(String desc) {
		boolean check = false;

		if (desc.toUpperCase().contains("PAYROLL") || desc.toUpperCase().contains("REFUND")
				|| desc.toUpperCase().contains("REBATE")) {
			check = true;
		}

		return check;
	}

}
