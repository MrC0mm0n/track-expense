package com.track.expense;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.track.expense.chart.PieChartData;
import com.track.expense.model.Loan;
import com.track.expense.model.Person;
import com.track.expense.repository.LoanRepository;
import com.track.expense.repository.PersonRepository;

@Controller
public class LoanController {

	private final Logger _logger = LoggerFactory.getLogger(LoanController.class);

	final String navigation = "loans";

	@Autowired
	LoanRepository loanRepo;

	@Autowired
	PersonRepository personRepo;

	List<Person> lPerson;

	// color, highlight
	String[][] colors = { { "#e6194B", "#ea6485" }, { "#3cb44b", "#8ee299" }, { "#ffe119", "#eddd71" }, { "#4363d8", "#7d94e8" }, { "#f58231", "#f7a871" }, { "#911eb4", "#be6fd6" },
			{ "#46f0f0", "#95f4f4" }, { "#f032e6", "#ed9ce8" }, { "#bcf60c", "#d1ea85" }, { "#f99595", "#fabebe" }, { "#008080", "#2bc6c6" }, { "#e6beff", "#ead9f4" }, { "#9A6324", "#c69863" },
			{ "#fffac8", "#e8e5cc" }, { "#800000", "#a34646" }, { "#aaffc3", "#dbf9e4" }, { "#808000", "#a0a026" }, { "#ffd8b1", "#f7e7d7" }, { "#000075", "#4e4e93" }, { "#e0d428", "#ede682" } };

	@RequestMapping(value = navigation)
	public ModelAndView defaultView(ModelAndView modelView) {
		_logger.debug(">> defaultView()");

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< defaultView()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save-loan")
	public ModelAndView saveLoan(ModelAndView modelView, com.track.expense.form.Loan formLoan) {
		_logger.debug(">> saveLoan()");

		_logger.debug("-- saving: " + formLoan.getSaveLoanId());

		Loan loan = new Loan();
		loan.setId(formLoan.getSaveLoanId());
		loan.setName(formLoan.getName());
		loan.setAmount(formLoan.getAmount());
		loan.setPerson(formLoan.getPerson());
		loan.setPayoffDate(formLoan.getPayoffDate());

		loan = loanRepo.save(loan);
		modelView.addObject("loanSaveSuccess", true);

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< saveLoan()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete-loan")
	public ModelAndView deleteLoan(ModelAndView modelView, com.track.expense.form.Loan loan) {
		_logger.debug(">> deleteLoan()");

		_logger.debug("-- deleting: " + loan.getDeleteLoanId());
		loanRepo.delete(loan.getDeleteLoanId());
		modelView.addObject("loanDeleteSuccess", true);

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< deleteLoan()");
		return modelView;
	}

	private ModelAndView constructDefaultModelView(ModelAndView modelView) {
		_logger.debug(">> constructModelView()");

		List<Loan> lLoan = loanRepo.findAll();
		modelView.addObject("lLoan", lLoan);

		modelView.addObject("saveLoan", new com.track.expense.form.Loan());
		modelView.addObject("deleteLoan", new com.track.expense.form.Loan());

		lPerson = personRepo.findAll();
		modelView.addObject("lPerson", lPerson);

		modelView = constructPieChartData(modelView);

		modelView.addObject("navigation", navigation);
		modelView.setViewName(navigation);

		_logger.debug("<< constructModelView()");
		return modelView;
	}

	private ModelAndView constructPieChartData(ModelAndView modelView) {
		_logger.debug(">> constructPieChartData()");

		List<PieChartData> lLoansByPerson = buildChartDataByPerson();
		modelView.addObject("lLoansByPerson", lLoansByPerson);
		try {
			modelView.addObject("jsonDataByPerson", new ObjectMapper().writeValueAsString(lLoansByPerson));
		} catch (JsonProcessingException e) {
			_logger.error("-- " + e.getMessage());
		}

		_logger.debug("<< constructPieChartData()");
		return modelView;
	}

	private List<PieChartData> buildChartDataByPerson() {
		_logger.debug(">> buildChartDataByPerson()");

		int i = 0;
		List<PieChartData> lIncomesByPerson = new ArrayList<PieChartData>();
		for (Person person : lPerson) {
			_logger.debug("-- " + person.getId() + ", " + person.getName());

			PieChartData p = new PieChartData();

			p.setColor(colors[i][0]);
			p.setHighlight(colors[i][1]);
			p.setLabel(person.getName());

			Double amount = 0D;
			for (Loan l : loanRepo.getByPerson(person.getId())) {
				amount = l.getAmount() + amount;
			}
			p.setValue(Precision.round(amount, 2));

			lIncomesByPerson.add(p);

			i++;
		}

		try {
			_logger.debug("-- " + new ObjectMapper().writeValueAsString(lIncomesByPerson));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		_logger.debug("<< buildChartDataByPerson()");
		return lIncomesByPerson;
	}

}
