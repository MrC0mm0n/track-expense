package com.track.expense;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.math3.util.Precision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.track.expense.chart.PieChartData;
import com.track.expense.form.SearchIncome;
import com.track.expense.model.Income;
import com.track.expense.model.IncomeCategory;
import com.track.expense.model.Person;
import com.track.expense.repository.IncomeCategoryRepository;
import com.track.expense.repository.IncomeRepository;
import com.track.expense.repository.PersonRepository;

@Controller
public class IncomeController {

	private final Logger _logger = LoggerFactory.getLogger(IncomeController.class);

	final String navigation = "income";

	@Autowired
	PersonRepository personRepo;

	@Autowired
	IncomeRepository incomeRepo;

	@Autowired
	IncomeCategoryRepository incomeCategoryRepo;

	List<Person> lPerson;
	List<IncomeCategory> lIncomeCategory;

	@RequestMapping(value = navigation)
	public ModelAndView defaultView(ModelAndView modelView) {
		_logger.debug(">> defaultView()");

		modelView = constructDefaultModelView(modelView);

		modelView = searchLast30Days(modelView);

		_logger.debug("<< defaultView()");
		return modelView;
	}

	@RequestMapping(value = "/search-income")
	public ModelAndView searchIncome(ModelAndView modelView, SearchIncome searchIncome) {
		_logger.debug(">> searchIncome()");

		modelView = constructDefaultModelView(modelView);

		List<Income> lIncomes = incomeRepo.searchIncomes(searchIncome);
		modelView.addObject("lIncomes", lIncomes);

		modelView.addObject("searchIncome", searchIncome);

		modelView = constructPieChartData(modelView, searchIncome);

		_logger.debug("<< searchIncome()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/save-income")
	public ModelAndView saveIncome(ModelAndView modelView, com.track.expense.form.Income formIncome) {
		_logger.debug(">> saveIncome()");

		_logger.debug("-- saving: " + formIncome.getSaveIncomeId());

		Income income = new Income();
		income.setId(formIncome.getSaveIncomeId());
		income.setDateTime(formIncome.getDateTime());
		income.setDescription(formIncome.getDescription());
		income.setAmount(formIncome.getAmount());
		income.setIncomeCategory(formIncome.getIncomeCategory());
		income.setPerson(formIncome.getPerson());

		income = incomeRepo.save(income);
		modelView.addObject("incomeSaveSuccess", true);

		modelView = constructDefaultModelView(modelView);

		modelView = searchLast30Days(modelView);

		_logger.debug("<< saveIncome()");
		return modelView;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/delete-income")
	public ModelAndView deleteIncome(ModelAndView modelView, com.track.expense.form.Income income) {
		_logger.debug(">> deleteIncome()");

		_logger.debug("-- deleting: " + income.getDeleteIncomeId());
		incomeRepo.delete(income.getDeleteIncomeId());
		modelView.addObject("incomeDeleteSuccess", true);

		modelView = constructDefaultModelView(modelView);

		modelView = searchLast30Days(modelView);

		_logger.debug("<< deleteIncome()");
		return modelView;
	}

	private ModelAndView constructDefaultModelView(ModelAndView modelView) {
		_logger.debug(">> constructModelView()");

		modelView.addObject("saveIncome", new com.track.expense.form.Income());
		modelView.addObject("deleteIncome", new com.track.expense.form.Income());

		lPerson = personRepo.findAll();
		modelView.addObject("lPerson", lPerson);

		lIncomeCategory = incomeCategoryRepo.findAll();
		modelView.addObject("lIncomeCategory", lIncomeCategory);

		modelView.addObject("navigation", navigation);
		modelView.setViewName(navigation);

		_logger.debug("<< constructModelView()");
		return modelView;
	}

	private ModelAndView searchLast30Days(ModelAndView modelView) {
		_logger.debug(">> searchLast30Days()");

		List<Income> lIncomes = incomeRepo.searchLast30Days();
		modelView.addObject("lIncomes", lIncomes);

		SearchIncome searchIncome = new SearchIncome();
		Calendar cal = Calendar.getInstance();
		searchIncome.setToDate(cal.getTime());
		cal.add(Calendar.MONTH, -1);
		searchIncome.setFromDate(cal.getTime());
		modelView.addObject("searchIncome", searchIncome);

		modelView = constructPieChartData(modelView, searchIncome);

		_logger.debug("<< searchLast30Days()");
		return modelView;
	}

	private ModelAndView constructPieChartData(ModelAndView modelView, SearchIncome searchIncome) {
		_logger.debug(">> constructPieChartData()");

		List<PieChartData> lIncomesByCategory = buildChartDataByIncomeCategory(searchIncome);
		modelView.addObject("lIncomesByCategory", lIncomesByCategory);
		try {
			modelView.addObject("jsonDataByCategory", new ObjectMapper().writeValueAsString(lIncomesByCategory));
		} catch (JsonProcessingException e) {
			_logger.error("-- " + e.getMessage());
		}

		List<PieChartData> lIncomesByPerson = buildChartDataByPerson(searchIncome);
		modelView.addObject("lIncomesByPerson", lIncomesByPerson);
		try {
			modelView.addObject("jsonDataByPerson", new ObjectMapper().writeValueAsString(lIncomesByPerson));
		} catch (JsonProcessingException e) {
			_logger.error("-- " + e.getMessage());
		}

		_logger.debug("<< constructPieChartData()");
		return modelView;
	}

	private List<PieChartData> buildChartDataByIncomeCategory(SearchIncome searchIncome) {
		_logger.debug(">> buildChartDataByIncomeCategory()");

		int i = 0;
		List<PieChartData> lIncomesByCategory = new ArrayList<PieChartData>();
		for (IncomeCategory incomeCategory : lIncomeCategory) {
			_logger.debug("-- " + incomeCategory.getId() + ", " + incomeCategory.getName());

			PieChartData p = new PieChartData();

			p.setColor(Namespace.colors[i][0]);
			p.setHighlight(Namespace.colors[i][1]);
			p.setLabel(incomeCategory.getName());

			Double amount = 0D;
			for (Income e : incomeRepo.getByIncomeCategory(incomeCategory.getId(), searchIncome.getFromDate(), searchIncome.getToDate())) {
				amount = e.getAmount() + amount;
			}
			p.setValue(Precision.round(amount, 2));

			lIncomesByCategory.add(p);

			i++;
		}

		try {
			_logger.debug("-- " + new ObjectMapper().writeValueAsString(lIncomesByCategory));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		_logger.debug("<< buildChartDataByIncomeCategory()");
		return lIncomesByCategory;
	}

	private List<PieChartData> buildChartDataByPerson(SearchIncome searchIncome) {
		_logger.debug(">> buildChartDataByPerson()");

		int i = 0;
		List<PieChartData> lIncomesByPerson = new ArrayList<PieChartData>();
		for (Person person : lPerson) {
			_logger.debug("-- " + person.getId() + ", " + person.getName());

			PieChartData p = new PieChartData();

			p.setColor(Namespace.colors[i][0]);
			p.setHighlight(Namespace.colors[i][1]);
			p.setLabel(person.getName());

			Double amount = 0D;
			for (Income e : incomeRepo.getByPerson(person.getId(), searchIncome.getFromDate(), searchIncome.getToDate())) {
				amount = e.getAmount() + amount;
			}
			p.setValue(Precision.round(amount, 2));

			lIncomesByPerson.add(p);

			i++;
		}

		try {
			_logger.debug("-- " + new ObjectMapper().writeValueAsString(lIncomesByPerson));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		_logger.debug("<< buildChartDataByPerson()");
		return lIncomesByPerson;
	}

}
