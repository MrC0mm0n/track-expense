package com.track.expense;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.track.expense.chart.Chart;
import com.track.expense.chart.Data;
import com.track.expense.chart.Datasets;
import com.track.expense.model.ExpenseCategory;
import com.track.expense.model.IncomeCategory;
import com.track.expense.model.Person;
import com.track.expense.repository.ExpenseCategoryRepository;
import com.track.expense.repository.ExpenseRepository;
import com.track.expense.repository.IncomeCategoryRepository;
import com.track.expense.repository.IncomeRepository;
import com.track.expense.repository.PersonRepository;

/**
 * Handles requests for the application home page.
 */
@Controller
public class DashboardController {

	private final Logger _logger = LoggerFactory.getLogger(DashboardController.class);

	final String navigation = "dashboard";

	@Autowired
	PersonRepository personRepo;

	@Autowired
	ExpenseRepository expenseRepo;

	@Autowired
	ExpenseCategoryRepository expenseCategoryRepo;

	@Autowired
	IncomeRepository incomeRepo;

	@Autowired
	IncomeCategoryRepository incomeCategoryRepo;

	List<Person> lPerson;
	List<ExpenseCategory> lExpenseCategory;

	@RequestMapping(value = { "/", navigation })
	public ModelAndView defaultView(ModelAndView modelView) {
		_logger.debug(">> defaultView()");

		modelView = constructDefaultModelView(modelView);

		_logger.debug("<< defaultView()");
		return modelView;
	}

	private ModelAndView constructDefaultModelView(ModelAndView modelView) {
		_logger.debug(">> constructModelView()");

		modelView.addObject("lPerson", personRepo.findAll());

		modelView.addObject("navigation", navigation);
		modelView.setViewName(navigation);

		_logger.debug("<< constructModelView()");
		return modelView;
	}

	@RequestMapping(value = "/get-all-persons")
	public @ResponseBody List<Person> getAllPersons(ModelAndView modelView) {
		_logger.debug(">> getAllPersons()");

		List<Person> lPerson = personRepo.findAll();

		_logger.debug("<< getAllPersons()");
		return lPerson;
	}

	@RequestMapping(value = "/get-data-by-person")
	public @ResponseBody Chart getDataByPerson(@RequestParam Integer personId, @RequestParam @DateTimeFormat(pattern = "dd-MMM-yyyy") Date fromDate,
			@RequestParam @DateTimeFormat(pattern = "dd-MMM-yyyy") Date toDate) {
		_logger.debug(">> getDataByPerson()");

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

		_logger.debug("-- personId - " + personId + ", fromDate - " + sdf.format(fromDate) + ", toDate - " + sdf.format(toDate));
		Person person = personRepo.findOne(personId);

		Data data = new Data();

		data.setLabels(makeLabels(fromDate, toDate));

		data.setDatasets(makeDatasets(personId, fromDate, toDate));

		Chart barChart = new Chart();
		barChart.setTitleText("Income & Expense - " + person.getName());
		barChart.setData(data);
		barChart.setPersonId(personId);

		_logger.debug("<< getDataByPerson()");
		return barChart;
	}

	private Datasets[] makeDatasets(Integer personId, Date fromDate, Date toDate) {
		_logger.debug(">> makeDatasets()");

		Calendar cFrom = Calendar.getInstance();
		cFrom.setTime(fromDate);

		Calendar cTo = Calendar.getInstance();
		cTo.setTime(toDate);

		int diffMonth = getMonthsBetween(cFrom, cTo);

		List<ExpenseCategory> lExpCategory = expenseCategoryRepo.findAll();
		List<IncomeCategory> lIncCategory = incomeCategoryRepo.findAll();

		Datasets[] datasets = new Datasets[lExpCategory.size() + lIncCategory.size() + 1];

		_logger.debug("-- gathering expenses");
		int h = 0;
		for (ExpenseCategory expCategory : lExpCategory) {

			datasets[h] = new Datasets();
			cFrom.setTime(fromDate);
			Double[] data = new Double[diffMonth];

			for (int i = 0; i < diffMonth; i++, cFrom.add(Calendar.MONTH, 1)) {

				data[i] = expenseRepo.getSumByPeriodAndPersonAndCategory(cFrom.get(Calendar.MONTH) + 1, cFrom.get(Calendar.YEAR), personId, expCategory.getId());

				if (data[i] == null)
					data[i] = 0D;

			}

			datasets[h].setData(data);
			datasets[h].setLabel(expCategory.getName());
			datasets[h].setStack(1);
			datasets[h].setBackgroundColor(Namespace.colors[h][0]);

			h++;
		}

		_logger.debug("-- gathering income");
		for (IncomeCategory incCategory : lIncCategory) {

			datasets[h] = new Datasets();
			cFrom.setTime(fromDate);
			Double[] data = new Double[diffMonth];

			for (int i = 0; i < diffMonth; i++, cFrom.add(Calendar.MONTH, 1)) {

				data[i] = incomeRepo.getSumByPeriodAndPersonAndCategory(cFrom.get(Calendar.MONTH) + 1, cFrom.get(Calendar.YEAR), personId, incCategory.getId());

				if (data[i] == null)
					data[i] = 0D;

			}

			datasets[h].setData(data);
			datasets[h].setLabel(incCategory.getName());
			datasets[h].setStack(2);
			datasets[h].setBackgroundColor(Namespace.colors[h][0]);

			h++;
		}

		_logger.debug("-- gathering savings/deficit");
		datasets[h] = new Datasets();
		cFrom.setTime(fromDate);
		Double[] data = new Double[diffMonth];

		for (int i = 0; i < diffMonth; i++, cFrom.add(Calendar.MONTH, 1)) {

			data[i] = personRepo.getIncomeMinusExpenseByPeriodAndPerson(cFrom.get(Calendar.MONTH) + 1, cFrom.get(Calendar.YEAR), personId);

			if (data[i] == null)
				data[i] = 0D;

		}

		datasets[h].setData(data);
		datasets[h].setLabel("Savings/Deficit");
		datasets[h].setStack(3);
		datasets[h].setBackgroundColor(Namespace.colors[h][0]);
		h++;

		_logger.debug("<< makeDatasets()");
		return datasets;
	}

	private String[] makeLabels(Date fromDate, Date toDate) {
		_logger.debug(">> makeLabels()");

		Calendar cFrom = Calendar.getInstance();
		cFrom.setTime(fromDate);

		Calendar cTo = Calendar.getInstance();
		cTo.setTime(toDate);

		int diffMonth = getMonthsBetween(cFrom, cTo);

		String[] labels = new String[diffMonth];

		for (int i = 0; i < diffMonth; i++, cFrom.add(Calendar.MONTH, 1))
			labels[i] = cFrom.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH) + " " + cFrom.get(Calendar.YEAR);

		_logger.debug("<< makeLabels()");
		return labels;
	}

	private int getMonthsBetween(Calendar cFrom, Calendar cTo) {

		LocalDate jFrom = new LocalDate(cFrom.get(Calendar.YEAR), cFrom.get(Calendar.MONTH), cFrom.get(Calendar.DAY_OF_MONTH));
		LocalDate jTo = new LocalDate(cTo.get(Calendar.YEAR), cTo.get(Calendar.MONTH), cTo.get(Calendar.DAY_OF_MONTH));

		int diffMonth = Months.monthsBetween(jFrom, jTo).getMonths();
		_logger.debug("-- diffMonth - " + diffMonth);

		return diffMonth;
	}

}