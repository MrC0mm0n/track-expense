<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<title>Navigation</title>

<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

<!-- Bootstrap Validator theme -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />

<!-- DataTables CSS -->
<link rel="stylesheet" media="all" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">

<!-- Chart JS legend -->
<link rel="stylesheet" href="resources/chartsjs/legend.css">

<link rel="stylesheet" href="resources/css/navigation.css">
</head>
<body>

	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-collapse collapse" id="navbar">
				<ul class="nav navbar-nav">
					<li id="dashboard"><a href="dashboard">Dashboard</a></li>
					<li id="income"><a href="income">Income</a></li>
					<li id="expense"><a href="expense">Expense</a></li>
					<li id="loans"><a href="loans">Loans</a></li>
					<li id="#utility"><a href="#utility">Utility</a></li>
					<li id="members"><a href="members">Members</a></li>
					<li id="expense-category"><a href="expense-category">Expense Category</a></li>
					<li id="income-category"><a href="income-category">Income Category</a></li>
				</ul>
			</div>
			<input type="hidden" id="navigation" value="${navigation}" />
		</div>
	</nav>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Latest compiled and minified Bootstrap JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<!-- Moment JS required for bootstrap datetime-picker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
	<!-- Bootstrap datetime-picker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
	<!-- Bootstrap Validator JS -->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
	<!-- DataTables JS -->
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>

	<script src="resources/chartsjs/legend.js"></script>

	<script type="text/javascript" src="resources/js/navigation.js"></script>

</body>
</html>