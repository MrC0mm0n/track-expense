<%@ include file="init.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title>People</title>

</head>
<body>

	<%@ include file="navigation.jsp"%>

	<div class="container">

		<div class="row">
			<div class="col-md-3">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#add-person" onclick="addPerson()">Add Person</button>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<table class="table">
					<caption class="h4">People</caption>
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Email</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${lPerson}" var="list" varStatus="i">
							<tr>
								<td>${i.count}</td>
								<td>${list.name}</td>
								<td>${list.email}</td>
								<td><span class="glyphicon glyphicon-edit" aria-hidden="true" onclick="updatePerson('${list.id}','${list.name}','${list.email}')"
									data-toggle="modal" data-target="#add-person"></span> <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"
									onclick="confirmDeletePerson(${list.id})" data-toggle="modal" data-target="#confirm-delete-person"></span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		
	</div>
	
	<!-- Add person modal -->
	<div class="modal fade" id="add-person" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<form:form action="save-member" modelAttribute="savePerson" method="POST">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="add-update-person-title"></h4>
					</div>

					<div class="modal-body">
						
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Name</span>
								<form:input path="name" class="form-control" required="true" />
							</div>
						</div>
						
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">E-mail</span>
								<form:input path="email" class="form-control" required="true" />
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-primary" value="Save" />
						<form:hidden path="savePersonId" />
					</div>

				</form:form>

			</div>
		</div>
	</div>
	
	<!-- Confirm delete person modal -->
	<div class="modal fade" id="confirm-delete-person" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Confirm Delete Member</h4>
				</div>

				<div class="modal-body">
					<p>Are you sure you want to delete this member?</p>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="deletePerson()">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				</div>

			</div>
		</div>
	</div>

	<!-- Delete person modal -->
	<form:form action="delete-member" modelAttribute="deletePerson" method="POST" id="deletePerson">
		<form:hidden path="deletePersonId" />
	</form:form>

	<input type="hidden" id="personSaveSuccess" value="${personSaveSuccess}" />
	<div class="modal fade" id="add-person-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Member added successfully</div>
		</div>
	</div>
	<div class="modal fade" id="add-person-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when adding member</div>
		</div>
	</div>

	<input type="hidden" id="personDeleteSuccess" value="${personDeleteSuccess}" />
	<div class="modal fade" id="delete-person-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Member deleted successfully</div>
		</div>
	</div>
	<div class="modal fade" id="delete-person-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when deleting member</div>
		</div>
	</div>
	
	<script type="text/javascript" src="resources/js/members.js"></script>

</body>
</html>