<%@ include file="init.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title>Expense Category</title>

</head>
<body>

	<%@ include file="navigation.jsp"%>

	<div class="container">

		<div class="row">
			<div class="col-md-3">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#add-category" onclick="addCategory()">Add Expense Category</button>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<table class="table">
					<caption class="h4">Expense Categories</caption>
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${lExpenseCategory}" var="list" varStatus="i">
							<tr>
								<td>${i.count}</td>
								<td>${list.name}</td>
								<td><span class="glyphicon glyphicon-edit" aria-hidden="true" onclick="updateCategory('${list.id}','${list.name}')"
									data-toggle="modal" data-target="#add-category"></span> <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"
									onclick="confirmDeleteCategory(${list.id})" data-toggle="modal" data-target="#confirm-delete-category"></span></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

	</div>
	
	<!-- Add category modal -->
	<div class="modal fade" id="add-category" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<form:form action="save-expense-category" modelAttribute="saveCategory" method="POST">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="add-update-category-title"></h4>
					</div>

					<div class="modal-body">
						
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Name</span>
								<form:input path="name" class="form-control" required="true" />
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-primary" value="Save" />
						<form:hidden path="saveCategoryId" />
					</div>

				</form:form>

			</div>
		</div>
	</div>
	
	<!-- Confirm delete category modal -->
	<div class="modal fade" id="confirm-delete-category" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Confirm Delete Category</h4>
				</div>

				<div class="modal-body">
					<p>Are you sure you want to delete this category?</p>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="deleteCategory()">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				</div>

			</div>
		</div>
	</div>

	<!-- Delete category modal -->
	<form:form action="delete-expense-category" modelAttribute="deleteCategory" method="POST" id="deleteCategory">
		<form:hidden path="deleteCategoryId" />
	</form:form>

	<input type="hidden" id="categorySaveSuccess" value="${categorySaveSuccess}" />
	<div class="modal fade" id="add-category-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Category added successfully</div>
		</div>
	</div>
	<div class="modal fade" id="add-category-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when adding your category</div>
		</div>
	</div>

	<input type="hidden" id="categoryDeleteSuccess" value="${categoryDeleteSuccess}" />
	<div class="modal fade" id="delete-category-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Category deleted successfully</div>
		</div>
	</div>
	<div class="modal fade" id="delete-category-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when deleting your category</div>
		</div>
	</div>

	<script type="text/javascript" src="resources/js/expense-category.js"></script>

</body>
</html>