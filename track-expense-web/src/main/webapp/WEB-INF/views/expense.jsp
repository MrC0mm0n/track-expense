<%@ include file="init.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title>Expense</title>

</head>
<body>

	<%@ include file="navigation.jsp"%>

	<div class="container">

		<div class="row">
			<div class="col-md-3">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#add-expense" onclick="addExpense()">Add Expense</button>
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#search-expense">Search</button>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<h4 align="center">Expense List</h4>
				<table class="table" id="expense-table">
					<thead>
						<tr>
							<th>Date</th>
							<th>Category</th>
							<th>Description</th>
							<th>Spender</th>
							<th>Amount</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="totalOfExpense" value="${0}" />
						<c:forEach items="${lExpenses}" var="list">
							<c:set var="totalOfExpense" value="${totalOfExpense + list.amount}" />
							<tr>
								<td><fmt:formatDate pattern="dd-MMM-yyyy" value="${list.dateTime}" /></td>
								<td>${list.expenseCategory.name}</td>
								<td>${list.description}</td>
								<td>${list.person.name}</td>
								<td><fmt:formatNumber value="${list.amount}" type="currency" currencySymbol="" /></td>
								<td><span class="glyphicon glyphicon-edit" aria-hidden="true"
									onclick="updateExpense('${list.dateTime}','${list.description}','${list.amount}','${list.person.id}','${list.person.name}','${list.expenseCategory.id}','${list.expenseCategory.name}','${list.id}')"
									data-toggle="modal" data-target="#add-expense"></span> <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"
									onclick="confirmDeleteExpense(${list.id})" data-toggle="modal" data-target="#confirm-delete-expense"></span></td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>Total: <fmt:formatNumber value="${totalOfExpense}" type="currency" currencySymbol="" /></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

		<div class="row">
			<h4 align="center">Statistics (<fmt:formatDate pattern="dd-MMM-yyyy" value="${searchExpense.fromDate}" /> to <fmt:formatDate pattern="dd-MMM-yyyy" value="${searchExpense.toDate}" />)</h4>
			<div class="col-md-6">
				<canvas id="pie-chart-by-category"></canvas>
				<table class="table">
					<caption class="h4">Expenses by Category</caption>
					<thead>
						<tr>
							<th>Category</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="totalByCategory" value="${0}" />
						<c:forEach items="${lExpensesByCategory}" var="list">
							<c:set var="totalByCategory" value="${totalByCategory + list.value}" />
							<tr>
								<td>${list.label}</td>
								<td><fmt:formatNumber value="${list.value}" type="currency" currencySymbol="" /></td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th><fmt:formatNumber value="${totalByCategory}" type="currency" currencySymbol="" /></th>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="col-md-6">
				<canvas id="pie-chart-by-person"></canvas>
				<table class="table">
					<caption class="h4">Expenses by Person</caption>
					<thead>
						<tr>
							<th>Name</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="totalByPerson" value="${0}" />
						<c:forEach items="${lExpensesByPerson}" var="list">
							<c:set var="totalByPerson" value="${totalByPerson + list.value}" />
							<tr>
								<td>${list.label}</td>
								<td><fmt:formatNumber value="${list.value}" type="currency" currencySymbol="" /></td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th><fmt:formatNumber value="${totalByPerson}" type="currency" currencySymbol="" /></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

	<script type="text/javascript">
	
		function setupChartByCategory() {
			debug('>> setupChartByCategory()');

			var canvas = document.getElementById('pie-chart-by-category');
			var data = ${jsonDataByCategory};
			var ctx = new Chart(canvas.getContext('2d')).Pie(data, {});
			
			debug('<< setupChartByCategory()');
		}
		
		function setupChartByPerson() {
			debug('>> setupChartByPerson()');
			
			var canvas = document.getElementById('pie-chart-by-person');
			var data = ${jsonDataByPerson};
			var ctx = new Chart(canvas.getContext('2d')).Pie(data, {});
			
			debug('<< setupChartByPerson()');
		}
	
	</script>

	</div>

	<!-- Search expense modal -->
	<div class="modal fade" id="search-expense" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<form:form action="search-expense" modelAttribute="searchExpense">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Search Expense</h4>
					</div>

					<div class="modal-body">

						<div class="form-group">
							<div class="input-group date" id="search-from-date">
								<fmt:formatDate pattern="dd-MMM-yyyy" value="${searchExpense.fromDate}" var="fromDate" />
								<span class="input-group-addon">From</span>
								<form:input path="fromDate" class="form-control" required="true" value="${fromDate}" />
								<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>

						<div class="form-group">
							<div class="input-group date" id="search-to-date">
								<fmt:formatDate pattern="dd-MMM-yyyy" value="${searchExpense.toDate}" var="toDate" />
								<span class="input-group-addon">To</span>
								<form:input path="toDate" class="form-control" required="true" value="${toDate}" />
								<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>

						<div class="form-group">
							<div class="input-group dropdown">
								<span class="input-group-addon">Spender</span> <input id="search-person-name" class="form-control">
								<form:hidden path="searchPerson.id" />
								<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<c:forEach items="${lPerson}" var="list">
											<li><a href="#" onclick="loadSearchPerson('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
										</c:forEach>
									</ul>
								</span>
							</div>
						</div>

						<div class="form-group">
							<div class="input-group dropdown">
								<span class="input-group-addon">Category</span> <input id="search-category-name" class="form-control">
								<form:hidden path="searchExpenseCategory.id" />
								<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<c:forEach items="${lExpenseCategory}" var="list">
											<li><a href="#" onclick="loadSearchCategory('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
										</c:forEach>
									</ul>
								</span>
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" onclick="resetSearch()">Rest</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-primary" value="Search" />
					</div>

				</form:form>

			</div>
		</div>
	</div>

	<!-- Add expense modal -->
	<div class="modal fade" id="add-expense" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<form:form action="save-expense" modelAttribute="saveExpense" method="POST">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="add-update-expense-title"></h4>
					</div>

					<div class="modal-body">

						<div class="form-group">
							<div class="input-group date" id="add-expense-date">
								<span class="input-group-addon">Date</span>
								<form:input path="dateTime" class="form-control" required="true" />
								<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
						
						<div class="form-group">
							<div class="input-group dropdown">
								<span class="input-group-addon">Spender</span> <input id="person-name" class="form-control" required>
								<form:hidden path="person.id" />
								<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
									aria-expanded="false"> <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<c:forEach items="${lPerson}" var="list">
											<li><a href="#" onclick="loadPerson('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
										</c:forEach>
									</ul>
								</span>
							</div>
						</div>

						<div class="form-group">
							<div class="input-group dropdown">
								<span class="input-group-addon">Category</span> <input id="category-name" class="form-control" required>
								<form:hidden path="expenseCategory.id" />
								<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
									aria-expanded="false"> <span class="caret"></span>
								</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<c:forEach items="${lExpenseCategory}" var="list">
											<li><a href="#" onclick="loadCategory('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
										</c:forEach>
									</ul>
								</span>
							</div>
						</div>
						
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Amount</span>
								<form:input path="amount" class="form-control" required="true" type="number" step="0.01" />
							</div>
						</div>

						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Description</span>
								<form:input path="description" class="form-control" required="true" />
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-primary" value="Save" />
						<form:hidden path="saveExpenseId" />
					</div>

				</form:form>

			</div>
		</div>
	</div>

	<!-- Confirm delete expense modal -->
	<div class="modal fade" id="confirm-delete-expense" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Confirm Delete Expense</h4>
				</div>

				<div class="modal-body">
					<p>Are you sure you want to delete this expense?</p>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="deleteExpense()">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				</div>

			</div>
		</div>
	</div>

	<!-- Delete expense modal -->
	<form:form action="delete-expense" modelAttribute="deleteExpense" method="POST" id="deleteExpense">
		<form:hidden path="deleteExpenseId" />
	</form:form>

	<input type="hidden" id="expenseSaveSuccess" value="${expenseSaveSuccess}" />
	<div class="modal fade" id="add-expense-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Expense added successfully</div>
		</div>
	</div>
	<div class="modal fade" id="add-expense-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when adding your expense</div>
		</div>
	</div>

	<input type="hidden" id="expenseDeleteSuccess" value="${expenseDeleteSuccess}" />
	<div class="modal fade" id="delete-expense-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Expense deleted successfully</div>
		</div>
	</div>
	<div class="modal fade" id="delete-expense-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when deleting your expense</div>
		</div>
	</div>

	<script type="text/javascript" src="resources/js/expense.js"></script>
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>

</body>
</html>