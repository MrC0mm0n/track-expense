<%@ include file="init.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title>Income</title>

</head>
<body>

	<%@ include file="navigation.jsp"%>

	<div class="container">

		<div class="row">
			<div class="col-md-3">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#add-income" onclick="addIncome()">Add Income</button>
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#search-income">Search</button>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
		</div>
		
		<div class="row">
			<div class="col-md-12">
				<h4 align="center">Income List</h4>
				<table class="table" id="income-table">
					<thead>
						<tr>
							<th>Date</th>
							<th>Category</th>
							<th>Description</th>
							<th>Earner</th>
							<th>Amount</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="totalOfIncome" value="${0}" />
						<c:forEach items="${lIncomes}" var="list">
							<c:set var="totalOfIncome" value="${totalOfIncome + list.amount}" />
							<tr>
								<td><fmt:formatDate pattern="dd-MMM-yyyy" value="${list.dateTime}" /></td>
								<td>${list.incomeCategory.name}</td>
								<td>${list.description}</td>
								<td>${list.person.name}</td>
								<td><fmt:formatNumber value="${list.amount}" type="currency" currencySymbol="" /></td>
								<td><span class="glyphicon glyphicon-edit" aria-hidden="true"
									onclick="updateIncome('${list.dateTime}','${list.description}','${list.amount}','${list.person.id}','${list.person.name}','${list.incomeCategory.id}','${list.incomeCategory.name}','${list.id}')"
									data-toggle="modal" data-target="#add-income"></span> <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"
									onclick="confirmDeleteIncome(${list.id})" data-toggle="modal" data-target="#confirm-delete-income"></span></td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th></th>
							<th></th>
							<th>Total: <fmt:formatNumber value="${totalOfIncome}" type="currency" currencySymbol="" /></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
		
		<div class="row">
			<h4 align="center">Statistics (<fmt:formatDate pattern="dd-MMM-yyyy" value="${searchIncome.fromDate}" /> to <fmt:formatDate pattern="dd-MMM-yyyy" value="${searchIncome.toDate}" />)</h4>
			<div class="col-md-6">
				<canvas id="pie-chart-by-category"></canvas>
				<table class="table">
					<caption class="h4">Incomes by Category</caption>
					<thead>
						<tr>
							<th>Category</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="totalByCategory" value="${0}" />
						<c:forEach items="${lIncomesByCategory}" var="list">
							<c:set var="totalByCategory" value="${totalByCategory + list.value}" />
							<tr>
								<td>${list.label}</td>
								<td><fmt:formatNumber value="${list.value}" type="currency" currencySymbol="" /></td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th><fmt:formatNumber value="${totalByCategory}" type="currency" currencySymbol="" /></th>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="col-md-6">
				<canvas id="pie-chart-by-person"></canvas>
				<table class="table">
					<caption class="h4">Incomes by Person</caption>
					<thead>
						<tr>
							<th>Name</th>
							<th>Amount</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="totalByPerson" value="${0}" />
						<c:forEach items="${lIncomesByPerson}" var="list">
							<c:set var="totalByPerson" value="${totalByPerson + list.value}" />
							<tr>
								<td>${list.label}</td>
								<td><fmt:formatNumber value="${list.value}" type="currency" currencySymbol="" /></td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th>Total</th>
							<th><fmt:formatNumber value="${totalByPerson}" type="currency" currencySymbol="" /></th>
						</tr>
					</tfoot>
				</table>
			</div>
		</div>

	<script type="text/javascript">
	
		function setupChartByCategory() {
			debug('>> setupChartByCategory()');

			var canvas = document.getElementById('pie-chart-by-category');
			var data = ${jsonDataByCategory};
			var ctx = new Chart(canvas.getContext('2d')).Pie(data, {});
			
			debug('<< setupChartByCategory()');
		}
		
		function setupChartByPerson() {
			debug('>> setupChartByPerson()');
			
			var canvas = document.getElementById('pie-chart-by-person');
			var data = ${jsonDataByPerson};
			var ctx = new Chart(canvas.getContext('2d')).Pie(data, {});
			
			debug('<< setupChartByPerson()');
		}
	
	</script>
		
	</div>
	
	<!-- Search income modal -->
	<div class="modal fade" id="search-income" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<form:form action="search-income" modelAttribute="searchIncome">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title">Search Income</h4>
					</div>

					<div class="modal-body">

						<div class="form-group">
							<div class="input-group date" id="search-from-date">
								<fmt:formatDate pattern="dd-MMM-yyyy" value="${searchIncome.fromDate}" var="fromDate" />
								<span class="input-group-addon">From</span>
								<form:input path="fromDate" class="form-control" required="true" value="${fromDate}" />
								<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>

						<div class="form-group">
							<div class="input-group date" id="search-to-date">
								<fmt:formatDate pattern="dd-MMM-yyyy" value="${searchIncome.toDate}" var="toDate" />
								<span class="input-group-addon">To</span>
								<form:input path="toDate" class="form-control" required="true" value="${toDate}" />
								<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>

						<div class="form-group">
							<div class="input-group dropdown">
								<span class="input-group-addon">Earner</span> <input id="search-person-name" class="form-control">
								<form:hidden path="searchPerson.id" />
								<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
									aria-expanded="false"> <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<c:forEach items="${lPerson}" var="list">
											<li><a href="#" onclick="loadSearchPerson('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
										</c:forEach>
									</ul>
								</span>
							</div>
						</div>

						<div class="form-group">
							<div class="input-group dropdown">
								<span class="input-group-addon">Category</span> <input id="search-category-name" class="form-control">
								<form:hidden path="searchIncomeCategory.id" />
								<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
									aria-expanded="false"> <span class="caret"></span>
								</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<c:forEach items="${lIncomeCategory}" var="list">
											<li><a href="#" onclick="loadSearchCategory('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
										</c:forEach>
									</ul>
								</span>
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" onclick="resetSearch()">Rest</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-primary" value="Search" />
					</div>

				</form:form>

			</div>
		</div>
	</div>
	
	<!-- Add income modal -->
	<div class="modal fade" id="add-income" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<form:form action="save-income" modelAttribute="saveIncome" method="POST">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="add-update-income-title"></h4>
					</div>

					<div class="modal-body">

						<div class="form-group">
							<div class="input-group date" id="add-income-date">
								<span class="input-group-addon">Date</span>
								<form:input path="dateTime" class="form-control" required="true" />
								<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>
						
						<div class="form-group">
							<div class="input-group dropdown">
								<span class="input-group-addon">Earner</span> <input id="person-name" class="form-control" required>
								<form:hidden path="person.id" />
								<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
									aria-expanded="false"> <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<c:forEach items="${lPerson}" var="list">
											<li><a href="#" onclick="loadPerson('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
										</c:forEach>
									</ul>
								</span>
							</div>
						</div>

						<div class="form-group">
							<div class="input-group dropdown">
								<span class="input-group-addon">Category</span> <input id="category-name" class="form-control" required>
								<form:hidden path="incomeCategory.id" />
								<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
									aria-expanded="false"> <span class="caret"></span>
								</a>
									<ul class="dropdown-menu dropdown-menu-right">
										<c:forEach items="${lIncomeCategory}" var="list">
											<li><a href="#" onclick="loadCategory('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
										</c:forEach>
									</ul>
								</span>
							</div>
						</div>
						
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Amount</span>
								<form:input path="amount" class="form-control" required="true" type="number" step="0.01" />
							</div>
						</div>

						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Description</span>
								<form:input path="description" class="form-control" required="true" />
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-primary" value="Save" />
						<form:hidden path="saveIncomeId" />
					</div>

				</form:form>

			</div>
		</div>
	</div>
	
	<!-- Confirm delete income modal -->
	<div class="modal fade" id="confirm-delete-income" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Confirm Delete Income</h4>
				</div>

				<div class="modal-body">
					<p>Are you sure you want to delete this income?</p>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="deleteIncome()">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				</div>

			</div>
		</div>
	</div>

	<!-- Delete income modal -->
	<form:form action="delete-income" modelAttribute="deleteIncome" method="POST" id="deleteIncome">
		<form:hidden path="deleteIncomeId" />
	</form:form>

	<input type="hidden" id="incomeSaveSuccess" value="${incomeSaveSuccess}" />
	<div class="modal fade" id="add-income-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Income added successfully</div>
		</div>
	</div>
	<div class="modal fade" id="add-income-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when adding your income</div>
		</div>
	</div>

	<input type="hidden" id="incomeDeleteSuccess" value="${incomeDeleteSuccess}" />
	<div class="modal fade" id="delete-income-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Income deleted successfully</div>
		</div>
	</div>
	<div class="modal fade" id="delete-income-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when deleting your income</div>
		</div>
	</div>
	
	<script type="text/javascript" src="resources/js/income.js"></script>
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>

</body>
</html>