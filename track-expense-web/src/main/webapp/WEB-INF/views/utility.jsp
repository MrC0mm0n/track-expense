<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html>
<head>
<title>Utility</title>

<!-- Latest compiled and minified Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css">

<!-- Bootstrap Validator theme -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/css/bootstrapValidator.min.css" />

<!-- DataTables CSS -->
<link rel="stylesheet" media="all" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css">

<!-- Chart JS legend -->
<link rel="stylesheet" href="resources/chartsjs/legend.css">

</head>
<body>
	<%@ include file="navigation.jsp"%>

	<div class="container">

		<form:form action="extract-data" modelAttribute="extractData" enctype="multipart/form-data">
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="upload-file">Upload File</label> <input type="file" id="upload-file" name="upload-file" />
					</div>
				</div>
				<div class="col-md-3"></div>
				<div class="col-md-3"></div>
				<div class="col-md-3"></div>
			</div>

			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label for="bank">Select Bank</label>
						<form:select path="bank" items="${banks}" cssClass="form-control" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="accountType">Account Type</label>
						<form:select path="accountType" items="${accountType}" cssClass="form-control" />
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label for="person">Select Person</label>
						<form:select path="person" items="${persons}" cssClass="form-control" />
					</div>
				</div>
				<div class="col-md-3"></div>
			</div>

			<div class="row">
				<div class="col-md-3">
					<input class="btn btn-default" type="submit" value="Submit">
				</div>
				<div class="col-md-3"></div>
				<div class="col-md-3"></div>
				<div class="col-md-3"></div>
			</div>
		</form:form>

	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Latest compiled and minified Bootstrap JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<!-- Moment JS required for bootstrap datetime-picker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>
	<!-- Bootstrap datetime-picker -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>
	<!-- Bootstrap Validator JS -->
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.2/js/bootstrapValidator.min.js"></script>
	<!-- DataTables JS -->
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
	<!-- Chart JS -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>
	<script src="resources/chartsjs/legend.js"></script>

	<script type="text/javascript" src="resources/js/utility.js"></script>
</body>
</html>