<%@ include file="init.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title>Loans</title>

</head>
<body>

	<%@ include file="navigation.jsp"%>

	<div class="container">

		<div class="row">
			<div class="col-md-3">
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#add-loan" onclick="addLoan()">Add Loan</button>
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
			<div class="col-md-3"></div>
		</div>
		
		<div class="row">
			<div class="col-md-9">
				<h4 align="center">Loans</h4>
				<table class="table" id="loan-table">
					<thead>
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Amount</th>
							<th>Bearer</th>
							<th>Payoff Date</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="totalLoan" value="${0}" />
						<c:forEach items="${lLoan}" var="list" varStatus="i">
							<c:set var="totalLoan" value="${totalLoan + list.amount}" />
							<tr>
								<td>${i.count}</td>
								<td>${list.name}</td>
								<td><fmt:formatNumber value="${list.amount}" type="currency" currencySymbol="" /></td>
								<td>${list.person.name}</td>
								<td><fmt:formatDate pattern="dd-MMM-yyyy" value="${list.payoffDate}" /></td>
								<td><span class="glyphicon glyphicon-edit" aria-hidden="true" onclick="updateLoan('${list.id}','${list.name}','${list.amount}','${list.person.id}','${list.person.name}','${list.payoffDate}')"
									data-toggle="modal" data-target="#add-loan"></span> <span class="glyphicon glyphicon-remove-circle" aria-hidden="true"
									onclick="confirmDeleteLoan(${list.id})" data-toggle="modal" data-target="#confirm-delete-loan"></span></td>
							</tr>
						</c:forEach>
					</tbody>
					<tfoot>
						<tr>
							<th></th>
							<th></th>
							<th>Total: <fmt:formatNumber value="${totalLoan}" type="currency" currencySymbol="" /></th>
							<th></th>
							<th></th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			</div>
			<div class="col-md-3">
				<canvas id="pie-chart-by-person"></canvas>
			</div>
		</div>
		
	<script type="text/javascript">
		
		function setupChartByPerson() {
			debug('>> setupChartByPerson()');
			
			var canvas = document.getElementById('pie-chart-by-person');
			var data = ${jsonDataByPerson};
			var ctx = new Chart(canvas.getContext('2d')).Pie(data, {});
			
			debug('<< setupChartByPerson()');
		}
	
	</script>
		
	</div>
	
	<!-- Add loan modal -->
	<div class="modal fade" id="add-loan" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<form:form action="save-loan" modelAttribute="saveLoan" method="POST">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<h4 class="modal-title" id="add-update-loan-title"></h4>
					</div>

					<div class="modal-body">
						
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Name</span>
								<form:input path="name" class="form-control" required="true" />
							</div>
						</div>
						
						<div class="form-group">
							<div class="input-group">
								<span class="input-group-addon">Amount</span>
								<form:input path="amount" class="form-control" required="true" type="number" step="0.01" />
							</div>
						</div>
						
						<div class="form-group">
							<div class="input-group dropdown">
								<span class="input-group-addon">Bearer</span> <input id="person-name" class="form-control" required>
								<form:hidden path="person.id" />
								<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
									aria-expanded="false"> <span class="caret"></span></a>
									<ul class="dropdown-menu dropdown-menu-right">
										<c:forEach items="${lPerson}" var="list">
											<li><a href="#" onclick="loadPerson('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
										</c:forEach>
									</ul>
								</span>
							</div>
						</div>
						
						<div class="form-group">
							<div class="input-group date" id="add-loan-date">
								<span class="input-group-addon">Payoff Date</span>
								<form:input path="payoffDate" class="form-control" required="true" />
								<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></span>
							</div>
						</div>

					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<input type="submit" class="btn btn-primary" value="Save" />
						<form:hidden path="saveLoanId" />
					</div>

				</form:form>

			</div>
		</div>
	</div>
	
	<!-- Confirm delete loan modal -->
	<div class="modal fade" id="confirm-delete-loan" data-keyboard="false" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Confirm Delete Loan</h4>
				</div>

				<div class="modal-body">
					<p>Are you sure you want to delete this loan?</p>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" onclick="deleteLoan()">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				</div>

			</div>
		</div>
	</div>

	<!-- Delete loan modal -->
	<form:form action="delete-loan" modelAttribute="deleteLoan" method="POST" id="deleteLoan">
		<form:hidden path="deleteLoanId" />
	</form:form>

	<input type="hidden" id="loanSaveSuccess" value="${loanSaveSuccess}" />
	<div class="modal fade" id="add-loan-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Loan added successfully</div>
		</div>
	</div>
	<div class="modal fade" id="add-loan-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when adding your loan</div>
		</div>
	</div>

	<input type="hidden" id="loanDeleteSuccess" value="${loanDeleteSuccess}" />
	<div class="modal fade" id="delete-loan-success-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-success" role="alert" align="center">Loan deleted successfully</div>
		</div>
	</div>
	<div class="modal fade" id="delete-loan-fail-notification" role="dialog">
		<div class="modal-dialog">
			<div class="alert alert-danger" role="alert" align="center">Something went wrong when deleting your loan</div>
		</div>
	</div>
	
	<script type="text/javascript" src="resources/js/loan.js"></script>
	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.2/Chart.min.js"></script>

</body>
</html>