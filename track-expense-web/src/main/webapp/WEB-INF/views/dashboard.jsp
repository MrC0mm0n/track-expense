<%@ include file="init.jsp"%>

<!DOCTYPE html>
<html>
<head>
<title>Dashboard</title>

</head>
<body>

	<%@ include file="navigation.jsp"%>

	<div class="container">
  		
  		<div class="row">
  			<div class="col-md-3">
  				<div class="form-group">
					<div class="input-group dropdown">
						<span class="input-group-addon">Person</span> <input id="person-name" class="form-control" required> <input type="hidden" id="person-id">
						<span class="input-group-addon"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> <span class="caret"></span></a>
							<ul class="dropdown-menu dropdown-menu-right">
								<c:forEach items="${lPerson}" var="list">
									<li><a href="#" onclick="loadPerson('${list.id}','${list.name}')"><c:out value="${list.name}"></c:out> </a></li>
								</c:forEach>
							</ul>
						</span>
					</div>
				</div>
  			</div>
  			<div class="col-md-3">
  				<div class="form-group">
					<div class="input-group date" id="div-from-date">
						<span class="input-group-addon">From</span>
						<input id="from-date" class="form-control" required="true">
						<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
  			</div>
  			<div class="col-md-3">
  				<div class="form-group">
					<div class="input-group date" id="div-to-date">
						<span class="input-group-addon">To</span>
						<input id="to-date" class="form-control" required="true">
						<span class="input-group-addon"> <span class="glyphicon glyphicon-calendar"></span></span>
					</div>
				</div>
  			</div>
  			<div class="col-md-3">
  				<button type="button" class="btn btn-primary" onclick="refreshChart()">Load</button>
  			</div>
  		</div>
  		
   		<canvas id="bar-chart" width="100%" height="50%"></canvas>
		
		<div class="row">
			<div class="col-md-12">
				<img id="design-loading" src="resources/images/design-loading.gif">
			</div>
		</div>
		<div class="alert alert-danger" role="alert" id="div-error">
			<p>Ops! Something went wrong</p>
			<p id="error-status"></p>
			<p id="error-message"></p>
		</div>

	</div>

<script type="text/javascript" src="resources/js/dashboard.js"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
	debug('>> document.ready()');
	
	$('#design-loading').hide();
	$('#div-error').hide();
	
	$('#div-from-date').datetimepicker({
		format : 'DD-MMM-YYYY'
	});
	$('#div-to-date').datetimepicker({
		format : 'DD-MMM-YYYY'
	});

	debug('<< document.ready()');
});

function loadPerson(id, name) {
	debug('>> loadPerson()');

	debug('-- ' + id + ', ' + name);
	$('#person-id').val(id);
	$('#person-name').val(name);

	debug('<< loadPerson()');
}

function refreshChart() {
	debug('>> refreshChart()');
	
	$('#bar-chart').hide();
	$('#design-loading').show();
	
	var personId = $('#person-id').val();
	var fromDate = $('#from-date').val();
	var toDate = $('#to-date').val();
	debug('-- personId - ' + personId + ', fromDate - ' + fromDate + ', toDate - ' + toDate);
	
	if(personId != '' && fromDate != '' && toDate != '') {
		
		$.ajax({
			url : '/portfolio/get-data-by-person',		
			contentType : 'application/json',
			dataType : 'json',
			data : {
				personId : personId,
				fromDate : fromDate,
				toDate : toDate
			},
			success : function(b) {
				debug(b);
				
				$('#div-error').hide();
				$('#design-loading').hide();
				$('#bar-chart').show();
				
				debug('-- get canvas');
				var canvas = $('#bar-chart');

				debug('-- get data');
				var data = b.data;

				debug('-- plot chart');
				var ctx = new Chart(canvas, {
					type : 'bar',
					data : data,
				    options: {
				        scales: {
				        	xAxes: [{ stacked: true }],
				            yAxes: [{ stacked: true }]
				        },
				        title: {
				            display: true,
				            text: b.titleText
				        }
				    }
				});

			},
			error : function(jqXHR, status, error) {

				$('#div-error').show();
				$('#error-status').text(status);
				$('#error-message').text(error);

			}
		});
		
	}
	
	debug('<< refreshChart()');
}
</script>

</body>
</html>