$(document).ready(function() {
	debug('>> document.ready()');

	// Setup search fields
	$('#search-from-date').datetimepicker({
		format : 'DD-MMM-YYYY'
	});
	$('#search-to-date').datetimepicker({
		format : 'DD-MMM-YYYY'
	});
	resetSearch();
	
	// Setup add income modal
	$('#add-income-date').datetimepicker({
		format : 'DD-MMM-YYYY'
	});
	
	// 
	$('#income-table').DataTable();
	
	//
	if ($('#incomeSaveSuccess').val() == 'true')
		$('#add-income-success-notification').modal('show');
	else if ($('#incomeSaveSuccess').val() == 'false')
		$('#add-income-fail-notification').modal('show');

	if ($('#incomeDeleteSuccess').val() == 'true')
		$('#delete-income-success-notification').modal('show');
	else if ($('#incomeSaveSuccess').val() == 'false')
		$('#delete-income-fail-notification').modal('show');

	// console.log($('#incomeSaveSuccess').val());
	
	setupChartByCategory();
	setupChartByPerson();
	
	debug('<< document.ready()');
});

function addIncome() {
	debug('>> addIncome()');

	$('#add-update-income-title').html('Add Income');

	$('#dateTime').val('');
	$('#description').val('');
	$('#amount').val('');
	loadPerson('', '');
	loadCategory('', '');
	$('#saveIncomeId').val('');

	debug('<< addIncome()');
}

function updateIncome(date, desc, amount, personId, personName, categoryId, categoryName, incomeId) {
	debug('>> updateIncome()');

	$('#add-update-income-title').html('Update Income');

	date = moment(date).format("DD-MMM-YYYY");
	debug('-- ' + incomeId + ', ' + date + ', ' + desc + ', ' + amount);

	$('#dateTime').val(date);
	$('#description').val(desc);
	$('#amount').val(amount);
	loadPerson(personId, personName);
	loadCategory(categoryId, categoryName);
	$('#saveIncomeId').val(incomeId);

	debug('<< updateIncome()');
}

function confirmDeleteIncome(incomeId) {
	debug('>> confirmDeleteIncome()');

	debug('-- ' + incomeId);
	$('#deleteIncomeId').val(incomeId);

	debug('<< confirmDeleteIncome()');
}

function deleteIncome() {
	debug('>> deleteIncome()');

	document.forms["deleteIncome"].submit();

	debug('<< deleteIncome()');
}

function loadPerson(id, name) {
	debug('>> loadPerson()');

	debug('-- ' + id + ', ' + name);
	$('#person\\.id').val(id);
	$('#person-name').val(name);

	debug('<< loadPerson()');
}

function loadCategory(id, name) {
	debug('>> loadCategory()');

	debug('-- ' + id + ', ' + name);
	$('#incomeCategory\\.id').val(id);
	$('#category-name').val(name);

	debug('<< loadCategory()');
}

function loadSearchPerson(id, name) {
	debug('>> loadPerson()');

	debug('-- ' + id + ', ' + name);
	$('#searchPerson\\.id').val(id);
	$('#search-person-name').val(name);

	debug('<< loadPerson()');
}

function loadSearchCategory(id, name) {
	debug('>> loadCategory()');

	debug('-- ' + id + ', ' + name);
	$('#searchIncomeCategory\\.id').val(id);
	$('#search-category-name').val(name);

	debug('<< loadCategory()');
}

function resetSearch() {
	debug('>> resetSearch()');
	
	$('#fromDate').val(moment(new Date($('#fromDate').val())).format("DD-MMM-YYYY"));
	$('#toDate').val(moment(new Date($('#toDate').val())).format("DD-MMM-YYYY"));
	loadSearchPerson('0', '');
	loadSearchCategory('0', '');
	
	debug('<< resetSearch()');
}

var debugMode = true;
function debug(msg) {
	if (debugMode)
		console.log(msg);
}