$(document).ready(function() {
	debug('>> document.ready()');
	
	// Setup add loan modal
	$('#add-loan-date').datetimepicker({
		format : 'DD-MMM-YYYY'
	});
	
	//
	if ($('#loanSaveSuccess').val() == 'true')
		$('#add-loan-success-notification').modal('show');
	else if ($('#loanSaveSuccess').val() == 'false')
		$('#add-loan-fail-notification').modal('show');

	if ($('#loanDeleteSuccess').val() == 'true')
		$('#delete-loan-success-notification').modal('show');
	else if ($('#loanSaveSuccess').val() == 'false')
		$('#delete-loan-fail-notification').modal('show');
	
	//
	setupChartByPerson();

	debug('<< document.ready()');
});

function addLoan() {
	debug('>> addLoan()');

	$('#add-update-loan-title').html('Add Loan');

	$('#name').val('');
	$('#amount').val('');
	loadPerson('', '');
	$('#saveLoanId').val('');

	debug('<< addLoan()');
}

function updateLoan(loanId, name, amount, personId, personName, payoffDate) {
	debug('>> updateLoan()');

	$('#add-update-loan-title').html('Update Loan');

	payoffDate = moment(payoffDate).format("DD-MMM-YYYY");
	debug('-- ' + loanId + ', ' + payoffDate + ', ' + name + ', ' + amount);

	$('#saveLoanId').val(loanId);
	$('#name').val(name);
	$('#amount').val(amount);
	loadPerson(personId, personName);
	$('#payoffDate').val(payoffDate);

	debug('<< updateLoan()');
}

function confirmDeleteLoan(loanId) {
	debug('>> confirmDeleteLoan()');

	debug('-- ' + loanId);
	$('#deleteLoanId').val(loanId);

	debug('<< confirmDeleteLoan()');
}

function deleteLoan() {
	debug('>> deleteLoan()');

	document.forms["deleteLoan"].submit();

	debug('<< deleteLoan()');
}

function loadPerson(id, name) {
	debug('>> loadPerson()');

	debug('-- ' + id + ', ' + name);
	$('#person\\.id').val(id);
	$('#person-name').val(name);

	debug('<< loadPerson()');
}

var debugMode = true;
function debug(msg) {
	if (debugMode)
		console.log(msg);
}