$(document).ready(function() {
	debug('>> document.ready()');

	// Setup search fields
	$('#search-from-date').datetimepicker({
		format : 'DD-MMM-YYYY'
	});
	$('#search-to-date').datetimepicker({
		format : 'DD-MMM-YYYY'
	});
	resetSearch();
	
	// Setup add expense modal
	$('#add-expense-date').datetimepicker({
		format : 'DD-MMM-YYYY'
	});

	// 
	$('#expense-table').DataTable();

	//
	if ($('#expenseSaveSuccess').val() == 'true')
		$('#add-expense-success-notification').modal('show');
	else if ($('#expenseSaveSuccess').val() == 'false')
		$('#add-expense-fail-notification').modal('show');

	if ($('#expenseDeleteSuccess').val() == 'true')
		$('#delete-expense-success-notification').modal('show');
	else if ($('#expenseSaveSuccess').val() == 'false')
		$('#delete-expense-fail-notification').modal('show');

	// console.log($('#expenseSaveSuccess').val());
	
	setupChartByCategory();
	setupChartByPerson();

	debug('<< document.ready()');
});

function addExpense() {
	debug('>> addExpense()');

	$('#add-update-expense-title').html('Add Expense');

	$('#dateTime').val('');
	$('#description').val('');
	$('#amount').val('');
	loadPerson('', '');
	loadCategory('', '');
	$('#saveExpenseId').val('');

	debug('<< addExpense()');
}

function updateExpense(date, desc, amount, personId, personName, categoryId, categoryName, expenseId) {
	debug('>> updateExpense()');

	$('#add-update-expense-title').html('Update Expense');

	date = moment(date).format("DD-MMM-YYYY");
	debug('-- ' + expenseId + ', ' + date + ', ' + desc + ', ' + amount);

	$('#dateTime').val(date);
	$('#description').val(desc);
	$('#amount').val(amount);
	loadPerson(personId, personName);
	loadCategory(categoryId, categoryName);
	$('#saveExpenseId').val(expenseId);

	debug('<< updateExpense()');
}

function confirmDeleteExpense(expenseId) {
	debug('>> confirmDeleteExpense()');

	debug('-- ' + expenseId);
	$('#deleteExpenseId').val(expenseId);

	debug('<< confirmDeleteExpense()');
}

function deleteExpense() {
	debug('>> deleteExpense()');

	document.forms["deleteExpense"].submit();

	debug('<< deleteExpense()');
}

function loadPerson(id, name) {
	debug('>> loadPerson()');

	debug('-- ' + id + ', ' + name);
	$('#person\\.id').val(id);
	$('#person-name').val(name);

	debug('<< loadPerson()');
}

function loadCategory(id, name) {
	debug('>> loadCategory()');

	debug('-- ' + id + ', ' + name);
	$('#expenseCategory\\.id').val(id);
	$('#category-name').val(name);

	debug('<< loadCategory()');
}

function loadSearchPerson(id, name) {
	debug('>> loadPerson()');

	debug('-- ' + id + ', ' + name);
	$('#searchPerson\\.id').val(id);
	$('#search-person-name').val(name);

	debug('<< loadPerson()');
}

function loadSearchCategory(id, name) {
	debug('>> loadCategory()');

	debug('-- ' + id + ', ' + name);
	$('#searchExpenseCategory\\.id').val(id);
	$('#search-category-name').val(name);

	debug('<< loadCategory()');
}

function resetSearch() {
	debug('>> resetSearch()');
	
	$('#fromDate').val(moment(new Date($('#fromDate').val())).format("DD-MMM-YYYY"));
	$('#toDate').val(moment(new Date($('#toDate').val())).format("DD-MMM-YYYY"));
	loadSearchPerson('0', '');
	loadSearchCategory('0', '');
	
	debug('<< resetSearch()');
}

var debugMode = true;
function debug(msg) {
	if (debugMode)
		console.log(msg);
}