$(document).ready(function() {
	debug('>> document.ready()');
	
	//
	if ($('#personSaveSuccess').val() == 'true')
		$('#add-person-success-notification').modal('show');
	else if ($('#personSaveSuccess').val() == 'false')
		$('#add-person-fail-notification').modal('show');

	if ($('#personDeleteSuccess').val() == 'true')
		$('#delete-person-success-notification').modal('show');
	else if ($('#personSaveSuccess').val() == 'false')
		$('#delete-person-fail-notification').modal('show');

	debug('<< document.ready()');
});

function addPerson() {
	debug('>> addPerson()');

	$('#add-update-person-title').html('Add Member');

	$('#name').val('');
	$('#amount').val('');

	debug('<< addPerson()');
}

function updatePerson(personId, name, email) {
	debug('>> updatePerson()');

	$('#add-update-person-title').html('Update Member');

	$('#savePersonId').val(personId);
	$('#name').val(name);
	$('#email').val(email);

	debug('<< updatePerson()');
}

function confirmDeletePerson(personId) {
	debug('>> confirmDeletePerson()');

	debug('-- ' + personId);
	$('#deletePersonId').val(personId);

	debug('<< confirmDeletePerson()');
}

function deletePerson() {
	debug('>> deletePerson()');

	document.forms["deletePerson"].submit();

	debug('<< deletePerson()');
}

var debugMode = true;
function debug(msg) {
	if (debugMode)
		console.log(msg);
}