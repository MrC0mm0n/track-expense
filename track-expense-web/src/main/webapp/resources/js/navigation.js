$(function() {

	if ($('#navigation').val() == 'dashboard')
		$('#dashboard').addClass('active');
	else if ($('#navigation').val() == 'income')
		$('#income').addClass('active');
	else if ($('#navigation').val() == 'expense')
		$('#expense').addClass('active');
	else if ($('#navigation').val() == 'loans')
		$('#loans').addClass('active');
	else if ($('#navigation').val() == 'members')
		$('#members').addClass('active');
	else if ($('#navigation').val() == 'expense-category')
		$('#expense-category').addClass('active');
	else if ($('#navigation').val() == 'income-category')
		$('#income-category').addClass('active');

});