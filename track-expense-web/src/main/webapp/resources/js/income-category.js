$(document).ready(function() {
	debug('>> document.ready()');
	
	//
	if ($('#categorySaveSuccess').val() == 'true')
		$('#add-category-success-notification').modal('show');
	else if ($('#categorySaveSuccess').val() == 'false')
		$('#add-category-fail-notification').modal('show');

	if ($('#categoryDeleteSuccess').val() == 'true')
		$('#delete-category-success-notification').modal('show');
	else if ($('#categorySaveSuccess').val() == 'false')
		$('#delete-category-fail-notification').modal('show');

	debug('<< document.ready()');
});

function addCategory() {
	debug('>> addCategory()');

	$('#add-update-category-title').html('Add Category');

	$('#saveCategoryId').val('');
	$('#name').val('');

	debug('<< addCategory()');
}

function updateCategory(categoryId, name) {
	debug('>> updateCategory()');

	$('#add-update-category-title').html('Update Category');

	debug('-- ' + categoryId + ', ' + name);

	$('#saveCategoryId').val(categoryId);
	$('#name').val(name);

	debug('<< updateCategory()');
}

function confirmDeleteCategory(categoryId) {
	debug('>> confirmDeleteCategory()');

	debug('-- ' + categoryId);
	$('#deleteCategoryId').val(categoryId);

	debug('<< confirmDeleteCategory()');
}

function deleteCategory() {
	debug('>> deleteCategory()');

	document.forms["deleteCategory"].submit();

	debug('<< deleteCategory()');
}

var debugMode = true;
function debug(msg) {
	if (debugMode)
		console.log(msg);
}