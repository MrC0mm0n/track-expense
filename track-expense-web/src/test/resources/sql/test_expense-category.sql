INSERT INTO
	EXPENSECATEGORY (id,name)
VALUES
	(1,'Grocery'),
	(2,'Necessities'),
	(3,'Shopping & Entertainment'),
	(4,'Education & Self-improvement'),
	(5,'Health & Well-being'),
	(6,'TV, Cell, Internet & Software'),
	(7,'Travel, Vehicle & Fuel'),
	(8,'Rent, Mortgage & Utilities'),
	(9,'Loan Repayments'),
	(10,'Restaurants'),
	(11,'Tax, Insurance & Interest'),
	(12,'Celebrations, Gifts & Charity'),
	(13,'Institutional Charges & Fees'),
	(14,'Others');