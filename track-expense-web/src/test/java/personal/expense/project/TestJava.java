package personal.expense.project;

import java.util.Calendar;
import java.util.Locale;

public class TestJava {

	public static void main(String[] args) {

		Calendar now = Calendar.getInstance();
		Calendar pastYear = Calendar.getInstance();
		pastYear.add(Calendar.YEAR, -1);

		System.out.println(now.getTime());
		System.out.println(pastYear.getTime());

		while (!pastYear.getTime().toString().equals(now.getTime().toString())) {
			System.out.println(pastYear.getDisplayName(Calendar.MONTH, Calendar.SHORT, Locale.ENGLISH) + " " + pastYear.get(Calendar.YEAR));

			pastYear.add(Calendar.MONTH, 1);
		}

	}

}
