package personal.expense.project;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.track.expense.model.ExpenseCategory;
import com.track.expense.model.Person;
import com.track.expense.repository.ExpenseCategoryRepository;
import com.track.expense.repository.ExpenseRepository;
import com.track.expense.repository.PersonRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/test/resources/db.xml" })
public class TestController {

	private static final Logger _logger = LoggerFactory.getLogger(TestController.class);

	@Autowired
	PersonRepository personRepo;

	@Autowired
	ExpenseRepository expenseRepo;

	@Autowired
	ExpenseCategoryRepository expenseCategoryRepo;

	List<Person> lPerson;
	List<ExpenseCategory> lExpenseCategory;

	// color, highlight
	String[][] colors = { { "#e6194B", "#ea6485" }, { "#3cb44b", "#8ee299" }, { "#ffe119", "#eddd71" }, { "#4363d8", "#7d94e8" }, { "#f58231", "#f7a871" }, { "#911eb4", "#be6fd6" },
			{ "#46f0f0", "#95f4f4" }, { "#f032e6", "#ed9ce8" }, { "#bcf60c", "#d1ea85" }, { "#f99595", "#fabebe" }, { "#008080", "#2bc6c6" }, { "#e6beff", "#ead9f4" }, { "#9A6324", "#c69863" },
			{ "#fffac8", "#e8e5cc" }, { "#800000", "#a34646" }, { "#aaffc3", "#dbf9e4" }, { "#808000", "#a0a026" }, { "#ffd8b1", "#f7e7d7" }, { "#000075", "#4e4e93" }, { "#e0d428", "#ede682" } };

	// @Before
	public void setUp() {
		_logger.info(">> setUp()");

		lPerson = new ArrayList<Person>();
		lPerson = personRepo.findAll();
		_logger.info("-- person.size: " + lPerson.size());

		lExpenseCategory = new ArrayList<ExpenseCategory>();
		lExpenseCategory = expenseCategoryRepo.findAll();
		_logger.info("-- expense-category.size: " + lExpenseCategory.size());

		_logger.info("<< setUp()");
	}

	// @Test
	public void testRepo() {
		_logger.info(">> testRepo()");

		_logger.info("<< testRepo()");
	}

	 @Test
	@SqlGroup({ @Sql(scripts = "file:src/test/resources/sql/test_expense-category.sql"), @Sql(scripts = "file:src/test/resources/sql/test_income-category.sql"),
			@Sql(scripts = "file:src/test/resources/sql/test_person.sql"), @Sql(scripts = "file:src/test/resources/sql/test_expense.sql"),
			@Sql(scripts = "file:src/test/resources/sql/test_income.sql"), @Sql(scripts = "file:src/test/resources/sql/test_loans.sql") })
	public void sqlwork() {
		_logger.info(">> sqlwork()");

		_logger.info("<< sqlwork()");
	}

}
